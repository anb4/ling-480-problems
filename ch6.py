# NLP Chapter 6 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
import random
from nltk.corpus import sentiwordnet as swn


# 1
# Read up on one of the language technologies mentioned in this section, such as word sense disambiguation, semantic role labeling, 
# question answering, machine translation, named entity detection. Find out what type and quantity of annotated data is required for 
# developing such systems. Why do you think a large amount of data is required?

# WSD: If your system wants to use a deep learning approach, it must have world knowledge.  Obviously this is a huge amount of data.
# It can also use a shallow approach, which just learns based on the training data without any other world knowledge.  Even to
# train a model to do WSD on a few words requires very large amounts of data because it must learn from context.  The smaller the
# training data, the less likely that the model is to find similar contexts in the testing data.

# 2
# Using any of the three classifiers described in this chapter, and any features you can think of, build the best name gender classifier
# you can. Begin by splitting the Names Corpus into three subsets: 500 words for the test set, 500 words for the dev-test set, and 
# the remaining 6900 words for the training set. Then, starting with the example name gender classifier, make incremental improvements.
# Use the dev-test set to check your progress. Once you are satisfied with your classifier, check its final performance on the test 
# set. How does the performance on the test set compare to the performance on the dev-test set? Is this what you'd expect?

def gender_features(name):
    features = {}
    features["last_letter"] = name[-1].lower()
    features["suffix2"] = name[-2].lower()
    features["last_vowel"] = name[-1].lower() in "aeiouy"

    return features

def prob_2():
	labeled_names = ([(name, 'male') for name in nltk.corpus.names.words('male.txt')] + [(name, 'female') for name in nltk.corpus.names.words('female.txt')])
	train_names = labeled_names[1000:]
	devtest_names = labeled_names[500:1000]
	test_names = labeled_names[:500]
	print(str(len(labeled_names)))

	train_set = [(gender_features(n), gender) for (n, gender) in train_names]
	devtest_set = [(gender_features(n), gender) for (n, gender) in devtest_names]
	test_set = [(gender_features(n), gender) for (n, gender) in test_names]
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, devtest_set))    # 0.684
	print(nltk.classify.accuracy(classifier, test_set))       # 0.638

# prob_2()
# The performance on the test set is slightly lower than the performance on the devtest set, but they are very close.
# This is expected because I catered the features to what worked on the devtest set.

# 3
# The Senseval 2 Corpus contains data intended to train word-sense disambiguation classifiers. It contains data for four words: hard, 
# interest, line, and serve. Choose one of these four words, and load the corresponding data: ...
# Using this dataset, build a classifier that predicts the correct sense tag for a given instance. See the corpus HOWTO at 
# http://nltk.org/howto for information on using the instance objects returned by the Senseval 2 Corpus.

def instance_features(senseval_instance):
    features = {}
    i = senseval_instance.position
    context = senseval_instance.context
    features["pos"] = context[i][1]

    if (i == 0):
    	features["prev_word"] = "<START>"
    	features["prev_pos"] = "<START>"
    else:
    	features["prev_word"] = context[i-1][0]
    	features["prev_pos"] = context[i-1][1]

    return features

def prob_3():
	instances = nltk.corpus.senseval.instances('hard.pos')
	size = int(len(instances) * 0.1)
	train_instances, test_instances = instances[size:], instances[:size]
	train_set = [(instance_features(senseval_instance), senseval_instance.senses) for senseval_instance in train_instances]
	test_set = [(instance_features(senseval_instance), senseval_instance.senses) for senseval_instance in test_instances]
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))     # 0.928406466513

# prob_3()

# 4
# Using the movie review document classifier discussed in this chapter, generate a list of the 30 features that the classifier finds 
# to be most informative. Can you explain why these particular features are informative? Do you find any of them surprising?
def document_features(document, word_features):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    return features

def prob_4():
	all_words = nltk.FreqDist(w.lower() for w in nltk.corpus.movie_reviews.words())
	word_features = list(all_words)[:2000]

	documents = [(list(nltk.corpus.movie_reviews.words(fileid)), category) for category in nltk.corpus.movie_reviews.categories() for fileid in nltk.corpus.movie_reviews.fileids(category)]
	random.shuffle(documents)
	featuresets = [(document_features(d, word_features), c) for (d,c) in documents]
	train_set, test_set = featuresets[100:], featuresets[:100]
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))    # 0.68
	classifier.show_most_informative_features(30)

# prob_4()
# Some of the informative features are emotionally charged themselves, like sunny, uplifting, effortlessly, tripe, and mediocrity.
# Other words seem to do with the subject of the movies, like attorney and leadership.
# Lastly, some of the reviews depend on the actors, as seen by words such as matheson, maxwell, and cronenberg.

# Most Informative Features
#           contains(sans) = True              neg : pos    =      8.3 : 1.0
#     contains(mediocrity) = True              neg : pos    =      7.6 : 1.0
#      contains(dismissed) = True              pos : neg    =      7.0 : 1.0
#      contains(uplifting) = True              pos : neg    =      6.2 : 1.0
#          contains(sunny) = True              pos : neg    =      5.7 : 1.0
#           contains(wits) = True              pos : neg    =      5.7 : 1.0
#    contains(overwhelmed) = True              pos : neg    =      5.7 : 1.0
#    contains(bruckheimer) = True              neg : pos    =      5.6 : 1.0
#      contains(sickening) = True              neg : pos    =      5.6 : 1.0
#          contains(wires) = True              neg : pos    =      5.6 : 1.0
#         contains(doubts) = True              pos : neg    =      5.0 : 1.0
#        contains(topping) = True              pos : neg    =      5.0 : 1.0
#         contains(fabric) = True              pos : neg    =      5.0 : 1.0
#      contains(testament) = True              pos : neg    =      5.0 : 1.0
#   contains(effortlessly) = True              pos : neg    =      5.0 : 1.0
#          contains(crowe) = True              pos : neg    =      4.6 : 1.0
#           contains(hugo) = True              pos : neg    =      4.6 : 1.0
#            contains(ugh) = True              neg : pos    =      4.6 : 1.0
#       contains(attorney) = True              pos : neg    =      4.6 : 1.0
#       contains(matheson) = True              pos : neg    =      4.4 : 1.0
#           contains(wang) = True              pos : neg    =      4.4 : 1.0
#           contains(lang) = True              pos : neg    =      4.4 : 1.0
#        contains(quicker) = True              neg : pos    =      4.3 : 1.0
#        contains(maxwell) = True              neg : pos    =      4.3 : 1.0
#          contains(locks) = True              neg : pos    =      4.3 : 1.0
#     contains(leadership) = True              pos : neg    =      4.2 : 1.0
#     contains(cronenberg) = True              pos : neg    =      4.2 : 1.0
#          contains(tripe) = True              neg : pos    =      4.2 : 1.0
#    contains(understands) = True              pos : neg    =      4.2 : 1.0
#        contains(admired) = True              pos : neg    =      4.2 : 1.0

# 5
# Select one of the classification tasks described in this chapter, such as name gender detection, document classification, 
# part-of-speech tagging, or dialog act classification. Using the same training and test data, and the same feature extractor,
# build three classifiers for the task: a decision tree, a naive Bayes classifier, and a Maximum Entropy classifier. Compare the 
# performance of the three classifiers on your selected task. How do you think that your results might be different if you used a 
# different feature extractor?

def run_naive_bayes(train_set, test_set):
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print "naive bayes:"
	print(nltk.classify.accuracy(classifier, test_set))      # ACCURACY: 0.638

def run_decision_tree(train_set, test_set):
	classifier = nltk.DecisionTreeClassifier.train(train_set)
	print "decision tree:"
	print(nltk.classify.accuracy(classifier, test_set))       # ACCURACY: 0.512

def run_max_entropy(train_set, test_set):
	algorithm = nltk.classify.MaxentClassifier.ALGORITHMS[0]
	classifier = nltk.MaxentClassifier.train(train_set, algorithm, max_iter=6)
	print "max entropy:"
	print(nltk.classify.accuracy(classifier, test_set))                        # ACCURACY: 0.524

def prob_5():
	labeled_names = ([(name, 'male') for name in nltk.corpus.names.words('male.txt')] + [(name, 'female') for name in nltk.corpus.names.words('female.txt')])
	train_names = labeled_names[1000:]
	devtest_names = labeled_names[500:1000]
	test_names = labeled_names[:500]

	train_set = [(gender_features(n), gender) for (n, gender) in train_names]
	devtest_set = [(gender_features(n), gender) for (n, gender) in devtest_names]
	test_set = [(gender_features(n), gender) for (n, gender) in test_names]

	run_naive_bayes(train_set, test_set)
	run_decision_tree(train_set, test_set)
	run_max_entropy(train_set, test_set)

# The accuracy should improve across the board as the feature extractor is more helpful.  Also, the max_iter of the max entropy
# classifier increases the accuracy as it is increased, but it takes longer to run.
# prob_5()

# 6
# The synonyms strong and powerful pattern differently (try combining them with chip and sales). What features are relevant in this 
# distinction? Build a classifier that predicts when each word should be used.

# Basing the feature off of the following, because I don't really understand the example provided here.  Strong chip?  Strong sales?
# These both seem fine to me.

# http://www.birmingham.ac.uk/Documents/college-artslaw/cels/essays/corpuslinguistics/A-Corpus-Study-of-Strong-and-Powerful.pdf
# In contrast, there seems to be a tendency towards strong being used in the form "strong and ADJ" or "strong, ADJ" 
# (e.g. strong, healthy adult or strong and infectious) than in the reverse formation. 

def word_features(sentence, i):
	features = {}
	if i == 0:
		features["prev-word"] = "<START>"
		features["prev-tag"] = "<START>"
	else:
		features["prev-word"] = sentence[i-1][0]
		features["prev-tag"] = sentence[i-1][1]
	if i == len(sentence) - 1:
		features["next-word"] = "<END>"
		features["next-tag"] = "<END>"
	else:
		features["next-word"] = sentence[i+1][0]
		features["next-tag"] = sentence[i+1][1]
	return features

def prob_6():
	all_example_sents = []
	brown_tagged_sents = nltk.corpus.brown.tagged_sents()
	for sent in brown_tagged_sents:
		for index, (word, tag) in enumerate(sent):
			if (word.lower() == 'strong') or (word.lower() is 'powerful'):
				all_example_sents.append((sent, index, word.lower()))
			break

	print str(len(all_example_sents))
	size = int(len(all_example_sents) * 0.9)
	train_sents = all_example_sents[size:]
	test_sents = all_example_sents[:size]

	train_set = [(word_features(sent, index), word) for (sent, index, word) in train_sents]
	test_set = [(word_features(sent, index), word) for (sent, index, word) in test_sents]

	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))

# prob_6()


# 7
# The dialog act classifier assigns labels to individual posts, without considering the context in which the post is found. However, 
# dialog acts are highly dependent on context, and some sequences of dialog act are much more likely than others. For example, a 
# ynQuestion dialog act is much more likely to be answered by a yanswer than by a greeting. Make use of this fact to build a 
# consecutive classifier for labeling dialog acts. Be sure to consider what features might be useful. See the code for the consecutive 
# classifier for part-of-speech tags in 1.7 to get some ideas.

def dialogue_act_features(post, history):
	features = {}
	for word in nltk.word_tokenize(post):
		features['contains({})'.format(word.lower())] = True
		if (len(history) == 0):
			features['prev-act'] = 'ynQuestion'
		else:
			features['prev-act'] = history[-1]
	return features


def prob_7():
	posts = nltk.corpus.nps_chat.xml_posts()[:10000]
	featuresets = []
	history = []
	for post in posts:
		featureset = dialogue_act_features(post.text, history)
		featuresets.append((featureset, post.get('class')))
		history.append(post.get('class'))

	# featuresets = [(dialogue_act_features(post.text), post.get('class')) for post in posts]
	size = int(len(featuresets) * 0.1)
	train_set, test_set = featuresets[size:], featuresets[:size]
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))           # 0.658

# prob_7()


# 8
# Word features can be very useful for performing document classification, since the words that appear in a document give a strong 
# indication about what its semantic content is. However, many words occur very infrequently, and some of the most informative words 
# in a document may never have occurred in our training data. One solution is to make use of a lexicon, which describes how different 
# words relate to one another. Using WordNet lexicon, augment the movie review document classifier presented in this chapter to use 
# features that generalize the words that appear in a document, making it more likely that they will match words found in the training 
# data.

# NOTE: this is using the SentiWordNet lexicon, because it is a better fit for this task.

def document_features(document, word_features):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    pos_score = 0
    neg_score = 0
    for word in document:
    	for senti_synset in swn.senti_synsets(word):
    		pos_score += senti_synset.pos_score()
    		neg_score += senti_synset.neg_score()
    if (pos_score > neg_score):
    	features['pos'] = True
    	features['neg'] = False
    elif (neg_score > pos_score):
    	features['pos'] = False
    	features['neg'] = True
    else:
    	features['pos'] = False
    	features['neg'] = False
    return features

def prob_8():
	all_words = nltk.FreqDist(w.lower() for w in nltk.corpus.movie_reviews.words())
	word_features = list(all_words)[:2000]

	documents = [(list(nltk.corpus.movie_reviews.words(fileid)), category) for category in nltk.corpus.movie_reviews.categories() for fileid in nltk.corpus.movie_reviews.fileids(category)]
	random.shuffle(documents)
	featuresets = [(document_features(d, word_features), c) for (d,c) in documents]
	train_set, test_set = featuresets[100:], featuresets[:100]
	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))    # 0.73

# prob_8()

# 9
# The PP Attachment Corpus is a corpus describing prepositional phrase attachment decisions. Each instance in the corpus is encoded as 
# a PPAttachment object.  Using this sub-corpus, build a classifier that attempts to predict which preposition is used to connect a 
# given pair of nouns. For example, given the pair of nouns "team" and "researchers," the classifier should predict the preposition 
# "of".

def nattach_features(inst):
	features = {}
	features['noun1'] = inst.noun1        # accuracy is higher when only using noun1
	# features['noun2'] = inst.noun2
	return features

def prob_9():
	nattach = [inst for inst in nltk.corpus.ppattach.attachments('training') if inst.attachment == 'N']
	nattach_test = [inst for inst in nltk.corpus.ppattach.attachments('test') if inst.attachment == 'N']

	train_set = [(nattach_features(inst), inst.prep) for inst in nattach]
	test_set = [(nattach_features(inst), inst.prep) for inst in nattach_test]

	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))     # 0.60843373494

# prob_9()

# 10
# Suppose you wanted to automatically generate a prose description of a scene, and already had a word to uniquely describe each 
# entity, such as the jar, and simply wanted to decide whether to use in or on in relating various items, e.g. the book is in the 
# cupboard vs the book is on the shelf. Explore this issue by looking at corpus data; writing programs as needed.

def nattach_features2(inst):
	features = {}
	features['noun2'] = inst.noun2
	return features

def prob_10():
	nattach = [inst for inst in nltk.corpus.ppattach.attachments('training') if inst.attachment == 'N' and (inst.prep == 'in' or inst.prep == 'on')]
	nattach_test = [inst for inst in nltk.corpus.ppattach.attachments('test') if inst.attachment == 'N' and (inst.prep == 'in' or inst.prep == 'on')]

	train_set = [(nattach_features2(inst), inst.prep) for inst in nattach]
	test_set = [(nattach_features2(inst), inst.prep) for inst in nattach_test]

	classifier = nltk.NaiveBayesClassifier.train(train_set)
	print(nltk.classify.accuracy(classifier, test_set))     # 0.6918767507

# prob_10()