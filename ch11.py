# NLP Chapter 11 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import ElementTree
import re
from collections import Counter
from itertools import dropwhile
import csv


# 1
# In 5.1 the new field appeared at the bottom of the entry. Modify this program so that it inserts the new subelement right 
# after the lx field. (Hint: create the new cv field using Element('cv'), assign a text value to it, then use the insert() 
#   method of the parent element.)

def cv(s):
    s = s.lower()
    s = re.sub(r'[^a-z]',     r'_', s)
    s = re.sub(r'[aeiou]',    r'V', s)
    s = re.sub(r'[^V_]',      r'C', s)
    return (s)

def add_cv_field(entry):
    for field in entry:
        if field.tag == 'lx':
            cv_field = Element('cv')
            cv_field.text = cv(field.text)
            entry.insert(1, cv_field)
            break
            
def prob_1():
    help(Element)
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    add_cv_field(lexicon[53])
    print(nltk.toolbox.to_sfm_string(lexicon[53]))

# prob_1()

# 2
# Write a function that deletes a specified field from a lexical entry. (We could use this to sanitize our lexical data 
#     before giving it to others, e.g. by removing fields containing irrelevant or uncertain content.)

def remove_field(entry):
    for field in entry:
        if field.tag == 'lx':
            entry.remove(field)
            break

def prob_2():
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    remove_field(lexicon[53])
    print(nltk.toolbox.to_sfm_string(lexicon[53]))

# prob_2()

# 3
# Write a program that scans an HTML dictionary file to find entries having an illegal part-of-speech field, and reports 
# the headword for each entry.
# say the html looks like this:
# <p class=MsoNormal>sleep
#   <b><span style='font-size:11.0pt'>v.i.</span></b>
#   <i>a condition of body and mind ...<o:p></o:p></i>
# </p>

def prob_3():
    illegal_pos = set(['n', 'v.t.', 'v.i.', 'adj', 'det'])
    pattern_n = re.compile(r"<p class=MsoNormal>([a-z.]+)<b><span style='font-size:11.0pt'>n<")
    pattern_vt = re.compile(r"<p class=MsoNormal>([a-z.]+)<b><span style='font-size:11.0pt'>v.t.<")
    pattern_vi = re.compile(r"<p class=MsoNormal>([a-z.]+)<b><span style='font-size:11.0pt'>v.i.<")
    pattern_adj = re.compile(r"<p class=MsoNormal>([a-z.]+)<b><span style='font-size:11.0pt'>adj<")
    pattern_det = re.compile(r"<p class=MsoNormal>([a-z.]+)<b><span style='font-size:11.0pt'>det<")

    document = open("dict.htm", encoding="windows-1252").read()     # this is the line that they provide, but it doesn't compile
    used_pos = set(re.findall(pattern, document))
    illegal_pos = used_pos.difference(legal_pos)
    print(list(illegal_pos))

# 4
# Write a program to find any parts of speech (ps field) that occurred less than ten times. Perhaps these are typing mistakes?

def get_pos(entry):
    for field in entry:
        if field.tag == 'ps':
            return field.text
            break

def prob_4():
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    short_lexicon = lexicon[:5]
    pos = []
    for lex in short_lexicon:
        pos.append(get_pos(lex))
    pos_freq = Counter(pos)
    # help(Counter)
    for key, value in pos_freq.items():
        if value < 10 and key != None:
            print key

# prob_4()

# 5
# We saw a method for discovering cases of whole-word reduplication. Write a function to find words that may contain 
# partial reduplication. Use the re.search() method, and the following regular expression:  (..+)\1

def prob_5(text):
    for word in text:
        match = re.search(r"(..+)\1", word)
        if match is not None:
            print word

text = ["hello", "world", "lalalala"]
# prob_5(text)

# 6
# We saw a method for adding a cv field. There is an interesting issue with keeping this up-to-date when someone modifies 
# the content of the lx field on which it is based. Write a version of this program to add a cv field, replacing any 
# existing cv field.

def add_update_cv_field(entry):
    found_cv = False
    for field in entry:
        # remove any existing cv field
        if field.tag == 'cv':
            entry.remove(field)
    for field in entry:
        # add a new cv field
        if field.tag == 'lx':
            cv_field = SubElement(entry, 'cv')
            cv_field.text = cv(field.text)
            
def prob_6():
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    add_update_cv_field(lexicon[53])
    # print(nltk.toolbox.to_sfm_string(lexicon[53]))
    add_update_cv_field(lexicon[53])

# prob_6()

# 7
# Write a function to add a new field syl which gives a count of the number of syllables in the word.

syl = {'kaeviro': '3'}   # could add here or use an outside dictionary, like cmudict, but that is for english only.

def add_syl_field(entry):
    for field in entry:
        if field.tag == 'lx':
            syl_field = SubElement(entry, 'syl')
            syl_field.text = syl[field.text]
            break

def prob_7():
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    add_syl_field(lexicon[53])
    print(nltk.toolbox.to_sfm_string(lexicon[53]))

# prob_7()

# 8
# Write a function which displays the complete entry for a lexeme.  When the lexeme is incorrectly spelled it should
# display the entry for the most similarly spelled lexeme.

mappings = [('ph', 'f'), ('ght', 't'), ('^kn', 'n'), ('qu', 'kw'), ('[aeiou]+', 'a'), (r'(.)\1', r'\1')]

def signature(word):
    for patt, repl in mappings:
        word = re.sub(patt, repl, word)
        pieces = re.findall('[^aeiou]+', word)
        return ''.join(char for piece in pieces for char in sorted(piece))[:8]

def rank(word, wordlist):
    ranked = sorted((nltk.edit_distance(word, w), w) for w in wordlist)
    return [word for (_, word) in ranked]

def fuzzy_spell(word):
    sig = signature(word)
    if sig in signatures:
        return rank(word, signatures[sig])
    else:
        return []

def display_entry(lexeme, entry):
    for field in entry:
        if field.tag == 'lx' and field.text == lexeme:
            return True

def prob_8(lexeme):
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    found = False
    for i in range(0, len(lexicon)):
        if display_entry(lexeme, lexicon[i]):
            print nltk.toolbox.to_sfm_string(lexicon[i])
            return
    if not found:
        # correct the spelling and try to find all the choices
        lexeme_choices = fuzzy_spell(lexeme)
        for lexeme in lexeme_choices:
                for i in range(0, len(lexicon)):
                    if display_entry(lexeme, lexicon[i]):
                        print nltk.toolbox.to_sfm_string(lexicon[i])
                        return
    print "Could not find lexeme or a close match."


# prob_8('kaa')

# 9
# Write a function that takes a lexicon and finds which pairs of consecutive fields are most frequent (e.g. ps is often 
# followed by pt). (This might help us to discover some of the structure of a lexical entry.)

def find_consec_fields(entry):
    fields = []
    first_iter = True
    for field in entry:
        if first_iter:
            first_iter = False
        else:
            fields.append((prev, field.tag))
        prev = field.tag
    return fields

def prob_9():
    long_lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    short_lexicon = long_lexicon[:10]
    consec_fields = []
    for i in range(0, len(short_lexicon)):
        consec_fields += find_consec_fields(short_lexicon[i])
    order = Counter(consec_fields)
    print order.most_common(5)

# prob_9()

# 10
# Create a spreadsheet using office software, containing one lexical entry per row, consisting of a headword, a part of 
# speech, and a gloss. Save the spreadsheet in CSV format. Write Python code to read the CSV file and print it in Toolbox 
# format, using lx for the headword, ps for the part of speech, and gl for the gloss.

def prob_10(filename):
    # reader = csv.reader(open(self.file, 'rU'), dialect=csv.excel_tab)
    with open(filename, 'rU') as csvfile:
        file_reader = csv.reader(csvfile, dialect='excel', delimiter=' ')

        root = Element("root")

        # ET.SubElement(doc, "field1", name="blah").text = "some value1"
        # ET.SubElement(doc, "field2", name="asdfasd").text = "some vlaue2"

        # tree = ET.ElementTree(root)
        # tree.write("filename.xml")

        for row in file_reader:
            print row
            entries = row[0].split(",")
            print entries
            headword = entries[0]
            pos = entries[1]
            gloss = entries[2]

            record = SubElement(root, "record")
            SubElement(record, "lx").text = headword
            SubElement(record, "pos").text = pos
            SubElement(record, "gl").text = gloss

        tree = ElementTree(root)
        tree.write("prob10.xml")

# prob_10("csvexample.csv")

# 11
# Index the words of Shakespeare's plays, with the help of nltk.Index. The resulting data structure should permit lookup 
# on individual words such as music, returning a list of references to acts, scenes and speeches, of the form 
# [(3, 2, 9), (5, 1, 23), ...], where (3, 2, 9) indicates Act 3 Scene 2 Speech 9.

def prob_11(search_word):
    merchant_file = nltk.data.find('corpora/shakespeare/merchant.xml')
    merchant = ElementTree().parse(merchant_file)

    idx = nltk.Index((word, (i, j, k))
                    for i, act in enumerate(merchant.findall('ACT'))
                    for j, scene in enumerate(act.findall('SCENE'))
                    for k, speech in enumerate(scene.findall('SPEECH'))
                    for line in speech.findall('LINE')
                    for word in str(line.text).split(" "))

    print idx[search_word]

# prob_11("music")

# 12
# Construct a conditional frequency distribution which records the word length for each speech in The Merchant of Venice, 
# conditioned on the name of the character, e.g. cfd['PORTIA'][12] would give us the number of speeches by Portia 
# consisting of 12 words.

def prob_12():
    merchant_file = nltk.data.find('corpora/shakespeare/merchant.xml')
    merchant = ElementTree().parse(merchant_file)

    len_speeches = [(speech[0].text, len(str(line.text).split(" ")))
                    for i, act in enumerate(merchant.findall('ACT'))
                    for j, scene in enumerate(act.findall('SCENE'))
                    for k, speech in enumerate(scene.findall('SPEECH'))
                    for line in speech.findall('LINE')]
    cfd = nltk.ConditionalFreqDist(len_speeches)

    print cfd['PORTIA'][12]

# prob_12()

# 13
# Obtain a comparative wordlist in CSV format, and write a program that prints those cognates having an edit-distance of 
# at least three from each other.

# NOTE: this edit distance function is not mine. Edit distance wasn't covered in the chapter so I thought it would be fine
# to reference someone else's solution.
# http://stackoverflow.com/questions/2460177/edit-distance-in-python
def edit_distance(s1, s2):
    m=len(s1)+1
    n=len(s2)+1

    tbl = {}
    for i in range(m): tbl[i,0]=i
    for j in range(n): tbl[0,j]=j
    for i in range(1, m):
        for j in range(1, n):
            cost = 0 if s1[i-1] == s2[j-1] else 1
            tbl[i,j] = min(tbl[i, j-1]+1, tbl[i-1, j]+1, tbl[i-1, j-1]+cost)

    return tbl[i,j]

def prob_13(filename):
    with open(filename, 'rU') as csvfile:
        file_reader = csv.reader(csvfile, dialect='excel', delimiter=' ')

        for row in file_reader:
            entries = row[0].split(",")
            word1 = entries[0]
            word2 = entries[1]
            if (edit_distance(word1, word2) >= 3):
                print (word1, word2)

# 14
# Build an index of those lexemes which appear in example sentences. Suppose the lexeme for a given entry is w. Then add a 
# single cross-reference field xrf to this entry, referencing the headwords of other entries having example sentences 
# containing w. Do this for all entries and save the result as a toolbox-format file.

def check_example(entry, lexeme):
    found = False
    for field in entry:
        if field.tag == 'lx':
            xrf_word = field.text
        elif field.tag == 'ex':
            sent = field.text
            if lexeme in sent:
                found = True
    if found:
        return xrf_word
    else:
        return None

def add_xrf_field(entry, lexicon):
    lexeme = ""
    for field in entry:
        if field.tag == 'lx':
            lexeme = field.text
            break
    for i in range(0, len(lexicon)):
        xrf_word = check_example(lexicon[i], lexeme)
        if (xrf_word):
            new_field = SubElement(entry, 'xrf')
            new_field.text = xrf_word            

def prob_14():
    lexicon = nltk.corpus.toolbox.xml('rotokas.dic')
    for i in range(0, len(lexicon)):
        add_xrf_field(lexicon[i], lexicon)

# prob_14()

# 15
# Write a recursive function to produce an XML representation for a tree, with non-terminals represented as XML elements, 
# and leaves represented as text content, e.g.:

# base case: only 2 parentheses in the whole thing
# recursive case: more than 2 parentheses

def prob_15(tree_str):
    open_p = "("
    close_p = ")"
    if (tree_str.count(open_p) + tree_str.count(close_p) <= 2):
        # base case: take stuff inside and split on space
        entry_info = tree_str[1:-1].split(" ")
        pos = entry_info[0]
        word = entry_info[1]
        return "<" + pos + ">" + word + "</" + pos + ">"
    else:
        entry_info = tree_str[1:-1]
        pos = entry_info[0]
        output = "<" + pos + ">\n\t"
        sub_entries = re.findall(r'\(\w+\s\w+\)', entry_info)
        for sub_entry in sub_entries:
            output += prob_15(sub_entry) + "\n\t"
        output += "\n</" + pos + ">" 
        return output

        # for things/entries inside it, concatenate all of the recursive return vals
tree = "(S (NP Mary) (VP saw))"
print prob_15(tree)
