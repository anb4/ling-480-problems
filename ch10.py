# NLP Chapter 10 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
import re

# 1
# Translate the following sentences into propositional logic and verify that they can be processed with 
# Expression.fromstring(). Provide a key which shows how the propositional variables in your translation correspond to 
# expressions of English.

def prob_1():
	read_expr = nltk.sem.Expression.fromstring

	# If Angus sings, it is not the case that Bertie sulks.
	# P = Angus sings
	# Q = Bertie sulks
	read_expr('P -> (-Q)')

	# Cyril runs and barks.
	# P = Cyril runs.
	# Q = Cyril barks.
	read_expr('P & Q')

	# It will snow if it doesn't rain.
	# P = it will rain
	# Q = it will snow
	read_expr('(-P) -> Q')

	# It's not the case that Irene will be happy if Olive or Tofu comes.
	# P = Olive comes
	# Q = Tofu comes
	# R = Irene will be happy
	read_expr('-((P | Q) -> R)')

	# Pat didn't cough or sneeze.
	# P = Pat coughed
	# Q = Pat sneezed
	read_expr('(-P) & (-Q)')

	# If you don't come if I call, I won't come if you call.
	# ((If I call) -> (you don't come)) then -> ((if you call) -> (I won't come))
	# P = I call
	# Q = You come
	# R = You call
	# S = I come
	read_expr('(P -> (-Q)) -> (R -> (-S))')

# prob_1()

# 2
# Translate the following sentences into predicate-argument formula of first order logic.
# NON QUANTIFIED (no existential or for all)

# Angus likes Cyril and Irene hates Cyril.
# likes(Angus, Cyril) & hates(Irene, Cyril)

# Tofu is taller than Bertie.
# taller(Tofu, Bertie)

# Bruce loves himself and Pat does too.
# love(Bruce, Bruce) & love(Pat, Pat)

# Cyril saw Bertie, but Angus didn't.
# saw(Cyril, Bertie) & -saw(Angus, Bertie)

# Cyril is a fourlegged friend.
# fourlegged_friend(Cyril)

# Tofu and Olive are near each other.
# near(Tofu, Olive) & near(Olive, Tofu)

# 3
# Translate the following sentences into quantified formulas of first order logic.

# Angus likes someone and someone likes Julia.
# exists x.likes(Angus, x) & exists y.likes(y, Julia)

# Angus loves a dog who loves him.
# exists x.(dog(x) & loves(Angus, x) & loves(x, Angus))

# Nobody smiles at Pat.
# all x.(-smiles(x, Pat))

# Somebody coughs and sneezes.
# exists x.(cough(x) & sneeze(x))

# Nobody coughed or sneezed.
# all x.(-cough(x) & -sneezed(x))

# Bruce loves somebody other than Bruce.
# exist x.(loves(Bruce, x) & (x != Bruce))

# Nobody other than Matthew loves Pat.
# all x.(((x = Matthew) & loves(x, Pat)) | ((x != Matthew) & -loves(x, Pat)))

# Cyril likes everyone except for Irene.
# all x.(((x = Irene) & (-likes(Cyril, x))) | ((x != Irene) & (likes(Cyril, x)))

# Exactly one person is asleep.
# (There exists one person x such that if any person y is asleep, that person is x)
# exists.x(all y.((asleep(y)) -> (y = x)))

# 4
# Translate the following verb phrases using lambda abstracts. quantified formulas of first order logic.

# feed Cyril and give a capuccino to Angus
# \x.(feed(x, Cyril) & give(x, Angus, capuccino))

# be given 'War and Peace' by Pat
# \x.(give(Pat, x, 'War and Peace'))

# be loved by everyone
# (to be an x such that x is loved by everyone)
# (to be an x such that for all y, y loves x)
# \x.all y.(loves(y, x))

# be loved or detested by everyone
# \x.all y.(loves(y, x) | detests(y, x))

# be loved by everyone and detested by no-one
# \x.all y.(loves(y, x) & (-detests(y, x)))

# 5
# Clearly something is missing here, namely a declaration of the value of e1. In order for ApplicationExpression(e1, e2) 
# to be b-convertible to exists y.love(pat, y), e1 must be a lambda-abstract which can take pat as an argument. Your task is to 
# construct such an abstract, bind it to e1, and satisfy yourself that the statements above are all satisfied (up to 
# 	alphabetic variance). In addition, provide an informal English translation of  e3.simplify().

def prob_5():
	read_expr = nltk.sem.Expression.fromstring
	e1 = read_expr(r'\x.exists y.(love(x, y))')
	e2 = read_expr('pat')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# exists y.love(pat, y)

	e1 = read_expr(r'\x.exists y.(love(x, y) | love(y, x))')
	e2 = read_expr('pat')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# exists y.(love(pat,y) | love(y,pat))

	e1 = read_expr(r'walk(fido)')
	e2 = read_expr('pat')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# walk(fido)
	# I don't think this one is possible, because applying e2 will always end up including pat somehow.
	# Trick question maybe?

# prob_5()

# 6
# As in the preceding exercise, find a lambda abstract e1 that yields results equivalent to those shown below.

def prob_6():
	read_expr = nltk.sem.Expression.fromstring
	e1 = read_expr(r'\X \x.(all y.(dog(y) -> X(x, pat)))')
	e2 = read_expr('chase')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# \x.all y.(dog(y) -> chase(x,pat))

	e1 = read_expr(r'\X \x.(exists y.(dog(y) -> X(pat, x)))')
	e2 = read_expr('chase')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# \x.exists y.(dog(y) & chase(pat,x))

	e1 = read_expr(r'\X \x0 \x1.(exists y.(present(y) & X(x1, y, x0)))')
	e2 = read_expr('give')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# \x0 x1.exists y.(present(y) & give(x1,y,x0))

# prob_6()

# 7
# As in the preceding exercise, find a lambda abstract e1 that yields results equivalent to those shown below.

def prob_7():
	read_expr = nltk.sem.Expression.fromstring
	e1 = read_expr(r'\X exists y.(dog(x) & X(x))')
	e2 = read_expr('bark')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# exists y.(dog(x) & bark(x))

	e1 = read_expr(r'\X.X(fido)')
	e2 = read_expr('bark')
	e3 = nltk.sem.ApplicationExpression(e1, e2)
	print(e3.simplify())
	# bark(fido)

	e1 = read_expr('bark')
	e2 = read_expr('\\P. all x. (dog(x) -> P(x))')
	e3 = nltk.sem.ApplicationExpression(e2, e1)
	print(e3.simplify())
	# all x.(dog(x) -> bark(x))

# prob_7()

# 8
# Develop a method for translating English sentences into formulas with binary generalized quantifiers. In such an approach, 
# given a generalized quantifier Q, a quantified formula is of the form Q(A, B), where both A and B are expressions of 
# type <e, t>. Then, for example, all(A, B) is true iff A denotes a subset of what B denotes.

# Given the next question, I think this question is asking just to output the "all" quantifiers.

def prob_8(sentence) :
	parser = nltk.load_parser('grammars/book_grammars/simple-sem.fcfg', trace=0)
	tokens = sentence.split()
	if ('all' in tokens):
		for tree in parser.parse(tokens):
			new_tree = tree.label()['SEM']
			tree_string = str(new_tree)
			pattern = re.compile("\w+\(\w+\)")
			match_list = re.findall(pattern, tree_string)
			gen_quantifier = 'all(' + match_list[0] + ", " + match_list[1] + ")"
			return gen_quantifier

# print prob_8("all dogs bark")

# 9
# Extend the approach in the preceding exercise so that the truth conditions for quantifiers like most and exactly three 
# can be computed in a model.

def prob_9(sentence) :
	parser = nltk.load_parser('grammars/book_grammars/simple-sem.fcfg', trace=0)
	tokens = sentence.split()
	for tree in parser.parse(tokens):
		new_tree = tree.label()['SEM']
		tree_string = str(new_tree)
		pattern = re.compile("\w+\(\w+\)")
		match_list = re.findall(pattern, tree_string)
		quantifier_list = []
		if ('all' in tokens):
			gen_quantifier = 'all(' + match_list[0] + ", " + match_list[1] + ")"
			quantifier_list.append(gen_quantifier)
		if ('some' in tokens):
			gen_quantifier = 'some(' + match_list[0] + ", " + match_list[1] + ")"
			quantifier_list.append(gen_quantifier)
		if ('exactly three' in tokens):
			gen_quantifier = 'exactlythree(' + match_list[0] + ", " + match_list[1] + ", " + match_list[2] + ")"
			quantifier_list.append(gen_quantifier)
		return quantifier_list

# print prob_9("all dogs bark")
# print prob_9("some dogs bark")

# 10
# Modify the sem.evaluate code so that it will give a helpful error message if an expression is not in the domain of a 
# model's valuation function.

# This i function is what is called from the satisfy method inside the sem.evaluate method.  This is where the checks 
# happen to make sure the variable is in the domain.

#@decorator(trace_eval)
def i(self, parsed, g, trace=False):
    """
    An interpretation function.

    Assuming that ``parsed`` is atomic:

    - if ``parsed`` is a non-logical constant, calls the valuation *V*
    - else if ``parsed`` is an individual variable, calls assignment *g*
    - else returns ``Undefined``.

    :param parsed: an ``Expression`` of ``logic``.
    :type g: Assignment
    :param g: an assignment to individual variables.
    :return: a semantic value
    """
    # If parsed is a propositional letter 'p', 'q', etc, it could be in valuation.symbols
    # and also be an IndividualVariableExpression. We want to catch this first case.
    # So there is a procedural consequence to the ordering of clauses here:
    if parsed.variable.name in self.valuation.symbols:
        return self.valuation[parsed.variable.name]
    elif isinstance(parsed, IndividualVariableExpression):
        return g[parsed.variable.name]
    elif parsed.variable.name not in self.valuation.symbols: # CHECKS IF THE NAME IS IN THE DOMAIN
    	raise Undefined("%s is not in the domain!" % parsed)
    else:
        raise Undefined("Can't find a value for %s" % parsed)

# 11
# Select three or four contiguous sentences from a book for children. A possible source of examples are the collections of 
# stories in nltk.corpus.gutenberg: bryant-stories.txt, burgess-busterbrown.txt and edgeworth-parents.txt. Develop a grammar 
# which will allow your sentences to be translated into first order logic, and build a model which will allow those 
# translations to be checked for truth or falsity.

# THIS IS WHAT THE GRAMMAR LOOKS LIKE:

# % start S
# ############################
# # Grammar Rules
# #############################

# S[SEM = <?subj(?vp)>] -> NP[NUM=?n,SEM=?subj] VP[NUM=?n,SEM=?vp]

# NP[NUM=?n,SEM=<?det(?nom)> ] -> Det[SEM=?det]  Nom[NUM=?n,SEM=?nom]
# NP[LOC=?l,NUM=?n,SEM=?np] -> PropN[LOC=?l,NUM=?n,SEM=?np]

# Nom[NUM=?n,SEM=?nom] -> N[SEM=?nom]

# VP[NUM=?n,SEM=<?v(?obj)>] -> TV[NUM=?n,SEM=?v] NP[SEM=?obj]
# VP[NUM=?n,SEM=<?v(?obj,?pp)>] -> IV[NUM=?n,SEM=?v] PP[+WITH,SEM=?pp]

# PP[+TO, SEM=?np] -> P[+TO] NP[SEM=?np]

# #############################
# # Lexical Rules
# #############################

# PropN[-LOC,NUM=sg,SEM=<\P.P(angus)>] -> 'Judy'
# PropN[-LOC,NUM=sg,SEM=<\P.P(cyril)>] -> 'Paul'

# Det[SEM=<\P Q.exists x.(P(x) & Q(x))>] -> 'the'

# N[SEM=<\x.bunny(x)>] -> 'bunny'
# N[SEM=<\x.she(x)>] -> 'she'

# TV[NUM=sg,SEM=<\X x.X(\y.pat(x,y))>,TNS=pres] -> 'pats'
# IV[NUM=sg,SEM=<\X x.X(\y.play(x,y))>,TNS=pres] -> 'plays'

# PP[+WITH, SEM=?np] -> P[+WITH] NP[SEM=?np]
# P[+WITH] -> 'with'

def prob_11(sentence):
	v = """
		Judy => j
		bunny => b
		Paul => p
		pats => {(j, b)}
		plays => (j)
		"""
	val = nltk.Valuation.fromstring(v)
	g = nltk.Assignment(val.domain)
	m = nltk.Model(val.domain, val)

	# parser = nltk.load_parser('grammars/book_grammars/simple-sem2.fcfg', trace=0)
	grammar_file = 'grammars/book_grammars/simple-sem2.fcfg' # location of parser
	results = nltk.evaluate_sents([sentence], grammar_file, m, g)[0]
	for (syntree, semrep, value) in results:
		print(semrep)
		print(value)
# sentences = ["Judy pats the bunny", "she plays with the bunny", "she plays with Paul"]


# 12
# Carry out the preceding exercise, but use DRT as the meaning representation.

# using the same grammar here as with above.
def prob_12(sentence):
	parser = nltk.load_parser('grammars/book_grammars/simple-sem2.fcfg', logic_parser=nltk.sem.drt.DrtParser()) # location of parser
	trees = list(parser.parse(sentence.split()))
	print(trees[0].label()['SEM'].simplify())
	
# 13
# Link is broken and it says to use that link and build off of that code.  Because I don't have the starting code,
# I will try to describe how I would approach the problem:
# Basically, the steps here would be to look at all of the expressions compared to each other recursively.  So, if we have
# (P(x) & Q(x)), we want to recurse on a list of [P(x), &, Q(x)] until we can compare the most basic forms of the
# expressions.  At this level, we check the extension of both against each other.  The one with the smaller extension 
# gets moved to the leftmost position.  We return this fixed ordering and as we recurse back up the tree, we continue
# to hold this rule as true by always putting the expression with the smaller extension on the left.

