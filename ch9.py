# NLP Chapter 9 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk

# 1
# What constraints are required to correctly parse word sequences like I am happy and she is happy but not *you is happy or 
# *they am happy? Implement two solutions for the present tense paradigm of the verb be in English, first taking Grammar (6)
#  as your starting point, and then taking Grammar (18) as the starting point.

# 6 as starting point
# organize by person and number
# S -> NP_1SG VP_SG
# S -> NP_1PL VP_PL
# S -> NP_2 VP_PL
# S -> NP_3SG VP_SG
# S -> NP_3PL VP_PL
# NP_1SG -> 'I'
# NP_1PL -> 'we'
# NP_2 -> 'you'
# NP_3SG -> 'he' | 'she' | 'it'
# NP_3PL -> 'they'

# VP_SG -> V_SG ADJ
# VP_PL -> V_PL ADJ

# V_SG -> 'is'
# V_PL -> 'are'
# ADJ -> 'happy'

# 18 as starting point
# S                    -> NP[AGR=?n] VP[AGR=?n]
# NP[AGR=?n]           -> N[AGR=?n]
# VP[AGR=?n] -> Cop[AGR=?n] Adj

# Cop[AGR=[NUM=sg, PER=3]] -> 'is'
# Cop[AGR=[NUM=sg, PER=1]] -> 'is'
# Cop[AGR=[NUM=sg, PER=2]] -> 'are'
# Cop[AGR=[NUM=pl, PER=3]] -> 'are'
# Cop[AGR=[NUM=pl, PER=2]] -> 'are'
# Cop[AGR=[NUM=pl, PER=1]] -> 'are'

# N[AGR=[NUM=sg, PER=3]] -> 'he' | 'she' | 'it'
# N[AGR=[NUM=sg, PER=2]] -> 'you'
# N[AGR=[NUM=sg, PER=1]] -> 'I'
# N[AGR=[NUM=pl, PER=3]] -> 'they'
# N[AGR=[NUM=pl, PER=2]] -> 'you'
# N[AGR=[NUM=pl, PER=1]] -> 'we'

# Adj -> 'happy'

# 2
# Develop a variant of grammar in 1.1 that uses a feature count to make the distinctions shown below:

# % start S
# # ###################
# # Grammar Productions
# # ###################
# # S expansion productions
# S -> NP[NUM=?n] VP[NUM=?n]
# # NP expansion productions
# NP[NUM=?n] -> N[NUM=?n]
# NP[NUM=?n] -> Det N[NUM=?n]
# NP[NUM=pl] -> N[NUM=pl]
# # VP expansion productions
# VP[NUM=?n] -> COP[NUM=?n] ADJ
# VP[NUM=?n] -> V[NUM=?n]
# # ###################
# # Lexical Productions
# # ###################
# Det -> 'the'
# N[NUM=sg] -> 'boy' | 'water'
# N[NUM=pl] -> 'boys'
# V[NUM=sg] -> 'sings'
# V[NUM=pl] -> 'sing'
# COP[NUM=sg] -> 'is'
# COP[NUM=pl] -> 'are'
# ADJ -> 'precious'

# 3
# Write a function subsumes() which holds of two feature structures fs1 and fs2 just in case fs1 subsumes fs2.

# Not entirely sure what this question is asking...
def subsumes(fs1, fs2):
    # saving copies in case it gets overwritten?
    fs1copy = fs1
    fs2copy = fs2
    fs3 = fs1.unify(fs2)
    if (fs3):
        return fs3
    else:
        return None

# 4
# Modify the grammar illustrated in (28) to incorporate a bar feature for dealing with phrasal projections.

# VP[TENSE=?t, NUM=?n, BAR=1] -> V[SUBCAT=intrans, TENSE=?t, NUM=?n, BAR=0]
# VP[TENSE=?t, NUM=?n, BAR=1] -> V[SUBCAT=trans, TENSE=?t, NUM=?n, BAR=0] NP
# VP[TENSE=?t, NUM=?n, BAR=1] -> V[SUBCAT=clause, TENSE=?t, NUM=?n, BAR=0] SBar

# V[SUBCAT=intrans, TENSE=pres, NUM=sg, BAR=0] -> 'disappears' | 'walks'
# V[SUBCAT=trans, TENSE=pres, NUM=sg, BAR=0] -> 'sees' | 'likes'
# V[SUBCAT=clause, TENSE=pres, NUM=sg, BAR=0] -> 'says' | 'claims'

# V[SUBCAT=intrans, TENSE=pres, NUM=pl, BAR=0] -> 'disappear' | 'walk'
# V[SUBCAT=trans, TENSE=pres, NUM=pl, BAR=0] -> 'see' | 'like'
# V[SUBCAT=clause, TENSE=pres, NUM=pl, BAR=0] -> 'say' | 'claim'

# V[SUBCAT=intrans, TENSE=past, NUM=?n, BAR=0] -> 'disappeared' | 'walked'
# V[SUBCAT=trans, TENSE=past, NUM=?n, BAR=0] -> 'saw' | 'liked'
# V[SUBCAT=clause, TENSE=past, NUM=?n, BAR=0] -> 'said' | 'claimed'

# 5
# Modify the German grammar in 3.2 to incorporate the treatment of subcategorization presented in 3.
# % start S
#  # Grammar Productions
#  S -> NP[CASE=nom, AGR=?a] VP[AGR=?a]
#  NP[CASE=?c, AGR=?a] -> PRO[CASE=?c, AGR=?a]
#  NP[CASE=?c, AGR=?a] -> Det[CASE=?c, AGR=?a] N[CASE=?c, AGR=?a]
#  VP[AGR=?a] -> IV[AGR=?a]
#  VP[AGR=?a] -> TV[OBJCASE=?c, AGR=?a] NP[CASE=?c]
#  SBar -> Comp S
#  Comp -> 'that' #(whatever this is in German)
#  VP[AGR=?a] -> V[CASE=clause, AGR=?a] SBar
# # and would also need to add verbs that work in a clause that look like this...
#  V[CASE=clause, AGR=[NUM=sg,PER=1]] -> '...' | '...'
# # with a bunch of these for agreement.

#  # Lexical Productions
#  # Singular determiners
#  # masc
#  Det[CASE=nom, AGR=[GND=masc,PER=3,NUM=sg]] -> 'der'
#  Det[CASE=dat, AGR=[GND=masc,PER=3,NUM=sg]] -> 'dem'
#  Det[CASE=acc, AGR=[GND=masc,PER=3,NUM=sg]] -> 'den'
#  # fem
#  Det[CASE=nom, AGR=[GND=fem,PER=3,NUM=sg]] -> 'die'
#  Det[CASE=dat, AGR=[GND=fem,PER=3,NUM=sg]] -> 'der'
#  Det[CASE=acc, AGR=[GND=fem,PER=3,NUM=sg]] -> 'die'
#  # Plural determiners
#  Det[CASE=nom, AGR=[PER=3,NUM=pl]] -> 'die'
#  Det[CASE=dat, AGR=[PER=3,NUM=pl]] -> 'den'
#  Det[CASE=acc, AGR=[PER=3,NUM=pl]] -> 'die'
#  # Nouns
#  N[AGR=[GND=masc,PER=3,NUM=sg]] -> 'Hund'
#  N[CASE=nom, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunde'
#  N[CASE=dat, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunden'
#  N[CASE=acc, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunde'
#  N[AGR=[GND=fem,PER=3,NUM=sg]] -> 'Katze'
#  N[AGR=[GND=fem,PER=3,NUM=pl]] -> 'Katzen'
#  # Pronouns
#  PRO[CASE=nom, AGR=[PER=1,NUM=sg]] -> 'ich'
#  PRO[CASE=acc, AGR=[PER=1,NUM=sg]] -> 'mich'
#  PRO[CASE=dat, AGR=[PER=1,NUM=sg]] -> 'mir'
#  PRO[CASE=nom, AGR=[PER=2,NUM=sg]] -> 'du'
#  PRO[CASE=nom, AGR=[PER=3,NUM=sg]] -> 'er' | 'sie' | 'es'
#  PRO[CASE=nom, AGR=[PER=1,NUM=pl]] -> 'wir'
#  PRO[CASE=acc, AGR=[PER=1,NUM=pl]] -> 'uns'
#  PRO[CASE=dat, AGR=[PER=1,NUM=pl]] -> 'uns'
#  PRO[CASE=nom, AGR=[PER=2,NUM=pl]] -> 'ihr'
#  PRO[CASE=nom, AGR=[PER=3,NUM=pl]] -> 'sie'
#  # Verbs
#  IV[AGR=[NUM=sg,PER=1]] -> 'komme'
#  IV[AGR=[NUM=sg,PER=2]] -> 'kommst'
#  IV[AGR=[NUM=sg,PER=3]] -> 'kommt'
#  IV[AGR=[NUM=pl, PER=1]] -> 'kommen'
#  IV[AGR=[NUM=pl, PER=2]] -> 'kommt'
#  IV[AGR=[NUM=pl, PER=3]] -> 'kommen'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=1]] -> 'sehe' | 'mag'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=2]] -> 'siehst' | 'magst'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=3]] -> 'sieht' | 'mag'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=1]] -> 'folge' | 'helfe'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=2]] -> 'folgst' | 'hilfst'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=3]] -> 'folgt' | 'hilft'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=1]] -> 'sehen' | 'moegen'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=2]] -> 'sieht' | 'moegt'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=3]] -> 'sehen' | 'moegen'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=1]] -> 'folgen' | 'helfen'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=2]] -> 'folgt' | 'helft'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=3]] -> 'folgen' | 'helfen'


# 6 -> Code isn't available in the textbook (System Message: ERROR/3 (ch09.rst2, line 2043))

# 7
# Develop your own version of the EarleyChartParser which only prints a trace if the input sequence fails to parse.

class newCP(nltk.parse.EarleyChartParser):
    def chart_parse(self, tokens, trace=None):
        if trace is None: trace = self._trace
        trace_new_edges = self._trace_new_edges

        tokens = list(tokens)
        self._grammar.check_coverage(tokens)
        chart = self._chart_class(tokens)
        grammar = self._grammar

        # Width, for printing trace edges.
        trace_edge_width = self._trace_chart_width // (chart.num_leaves() + 1)
        # CHANGE IS BE HERE::::
        if trace: print(chart.pretty_format_leaves(trace_edge_width))

        for axiom in self._axioms:
            new_edges = list(axiom.apply(chart, grammar))
            trace_new_edges(chart, axiom, new_edges, trace, trace_edge_width)

        inference_rules = self._inference_rules
        for end in range(chart.num_leaves()+1):
            if trace > 1: print("\n* Processing queue:", end, "\n")
            agenda = list(chart.select(end=end))
            while agenda:
                edge = agenda.pop()
                for rule in inference_rules:
                    new_edges = list(rule.apply(chart, grammar, edge))
                    trace_new_edges(chart, rule, new_edges, trace, trace_edge_width)
                    for new_edge in new_edges:
                        if new_edge.end()==end:
                            agenda.append(new_edge)

        return chart

# 8
# Consider the feature structures shown in 6.1. Work out on paper what the result is of the following unifications. 
# (Hint: you might find it useful to draw the graph structures.) Check your answers using Python.

def prob_8():
    fs1 = nltk.FeatStruct("[A = ?x, B= [C = ?x]]")
    fs2 = nltk.FeatStruct("[B = [D = d]]")
    fs3 = nltk.FeatStruct("[B = [C = d]]")
    fs4 = nltk.FeatStruct("[A = (1)[B = b], C->(1)]")
    fs5 = nltk.FeatStruct("[A = (1)[D = ?x], C = [E -> (1), F = ?x] ]")
    fs6 = nltk.FeatStruct("[A = [D = d]]")
    fs7 = nltk.FeatStruct("[A = [D = d], C = [F = [D = d]]]")
    fs8 = nltk.FeatStruct("[A = (1)[D = ?x, G = ?x], C = [B = ?x, E -> (1)] ]")
    fs9 = nltk.FeatStruct("[A = [B = b], C = [E = [G = e]]]")
    fs10 = nltk.FeatStruct("[A = (1)[B = b], C -> (1)]")

    # fs1 and fs2
    new_fs = fs1.unify(fs2)
    # print new_fs

    # fs1 and fs3
    new_fs = fs1.unify(fs3)
    # print new_fs

    # fs4 and fs5
    new_fs = fs4.unify(fs5)
    # print new_fs

    # fs5 and fs6
    new_fs = fs5.unify(fs6)
    # print new_fs

    # fs5 and fs7
    new_fs = fs5.unify(fs7)
    # print new_fs

    # fs8 and fs9
    new_fs = fs8.unify(fs9)
    # print new_fs

    # fs8 and fs10
    new_fs = fs8.unify(fs10)

    print new_fs

# prob_8()

# 9
# List two feature structures that subsume [A=?x, B=?x].
# fs1 = nltk.FeatStruct("[B= ?x]")
# fs2 = nltk.FeatStruct("[A= ?x]")

# 10
# Ignoring structure sharing, give an informal algorithm for unifying two feature structures.

# fs1.unify(fs2)
# fs3 is the unification, start with fs3=fs1
# for feature in fs1
    # recurse through that feature, checking at each stage that the feature in the same spot in fs2 has the same value
        # if it does have the same value, do nothing
        # if it does not have the same value, return None
    # if that feature does not exist in fs2, add it to fs3
# for feature in fs2
    # recurse through that feature, checking to make sure it exists in fs1
    # if that feature does not exist in fs1, add it to fs3
# return fs3

# 11
# Extend the German grammar in 3.2 so that it can handle so-called verb-second structures like the following:
# Heute sieht der Hund die Katze.
# heute = today, ADV

# % start S
#  # Grammar Productions
#  S -> NP[CASE=nom, AGR=?a] VP[AGR=?a]
#  NP[CASE=?c, AGR=?a] -> PRO[CASE=?c, AGR=?a]
#  NP[CASE=?c, AGR=?a] -> Det[CASE=?c, AGR=?a] N[CASE=?c, AGR=?a]
#  VP[AGR=?a] -> IV[AGR=?a]
#  VP[AGR=?a] -> ADV IV[AGR=?a]

#  VP[AGR=?a] -> TV[OBJCASE=?c, AGR=?a] NP[CASE=?c]
#  VP[AGR=?a] -> ADV TV[OBJCASE=?c, AGR=?a] NP[CASE=?c]

#  # Lexical Productions
#  # Singular determiners
#  # masc
#  Det[CASE=nom, AGR=[GND=masc,PER=3,NUM=sg]] -> 'der'
#  Det[CASE=dat, AGR=[GND=masc,PER=3,NUM=sg]] -> 'dem'
#  Det[CASE=acc, AGR=[GND=masc,PER=3,NUM=sg]] -> 'den'
#  # fem
#  Det[CASE=nom, AGR=[GND=fem,PER=3,NUM=sg]] -> 'die'
#  Det[CASE=dat, AGR=[GND=fem,PER=3,NUM=sg]] -> 'der'
#  Det[CASE=acc, AGR=[GND=fem,PER=3,NUM=sg]] -> 'die'
#  # Plural determiners
#  Det[CASE=nom, AGR=[PER=3,NUM=pl]] -> 'die'
#  Det[CASE=dat, AGR=[PER=3,NUM=pl]] -> 'den'
#  Det[CASE=acc, AGR=[PER=3,NUM=pl]] -> 'die'
#  # Nouns
#  N[AGR=[GND=masc,PER=3,NUM=sg]] -> 'Hund'
#  N[CASE=nom, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunde'
#  N[CASE=dat, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunden'
#  N[CASE=acc, AGR=[GND=masc,PER=3,NUM=pl]] -> 'Hunde'
#  N[AGR=[GND=fem,PER=3,NUM=sg]] -> 'Katze'
#  N[AGR=[GND=fem,PER=3,NUM=pl]] -> 'Katzen'
#  # Pronouns
#  PRO[CASE=nom, AGR=[PER=1,NUM=sg]] -> 'ich'
#  PRO[CASE=acc, AGR=[PER=1,NUM=sg]] -> 'mich'
#  PRO[CASE=dat, AGR=[PER=1,NUM=sg]] -> 'mir'
#  PRO[CASE=nom, AGR=[PER=2,NUM=sg]] -> 'du'
#  PRO[CASE=nom, AGR=[PER=3,NUM=sg]] -> 'er' | 'sie' | 'es'
#  PRO[CASE=nom, AGR=[PER=1,NUM=pl]] -> 'wir'
#  PRO[CASE=acc, AGR=[PER=1,NUM=pl]] -> 'uns'
#  PRO[CASE=dat, AGR=[PER=1,NUM=pl]] -> 'uns'
#  PRO[CASE=nom, AGR=[PER=2,NUM=pl]] -> 'ihr'
#  PRO[CASE=nom, AGR=[PER=3,NUM=pl]] -> 'sie'
#  # Verbs
#  IV[AGR=[NUM=sg,PER=1]] -> 'komme'
#  IV[AGR=[NUM=sg,PER=2]] -> 'kommst'
#  IV[AGR=[NUM=sg,PER=3]] -> 'kommt'
#  IV[AGR=[NUM=pl, PER=1]] -> 'kommen'
#  IV[AGR=[NUM=pl, PER=2]] -> 'kommt'
#  IV[AGR=[NUM=pl, PER=3]] -> 'kommen'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=1]] -> 'sehe' | 'mag'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=2]] -> 'siehst' | 'magst'
#  TV[OBJCASE=acc, AGR=[NUM=sg,PER=3]] -> 'sieht' | 'mag'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=1]] -> 'folge' | 'helfe'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=2]] -> 'folgst' | 'hilfst'
#  TV[OBJCASE=dat, AGR=[NUM=sg,PER=3]] -> 'folgt' | 'hilft'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=1]] -> 'sehen' | 'moegen'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=2]] -> 'sieht' | 'moegt'
#  TV[OBJCASE=acc, AGR=[NUM=pl,PER=3]] -> 'sehen' | 'moegen'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=1]] -> 'folgen' | 'helfen'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=2]] -> 'folgt' | 'helft'
#  TV[OBJCASE=dat, AGR=[NUM=pl,PER=3]] -> 'folgen' | 'helfen'


# 12
# Seemingly synonymous verbs have slightly different syntactic properties (Levin, 1993). Consider the patterns of 
# grammaticality for the verbs loaded, filled, and dumped below. Can you write grammar productions to handle such data?
# a.The farmer loaded the cart with sand
# b.The farmer loaded sand into the cart
# c.The farmer filled the cart with sand
# d.*The farmer filled sand into the cart
# e.*The farmer dumped the cart with sand
# f.The farmer dumped sand into the cart

# S -> NP VP
# NP -> DET N
# DET -> 'the'
# N -> 'farmer' | 'sand' | 'cart'
# VP -> V1 N PP1 | V1 N PP2
# VP -> V2 N PP2
# PP1 -> P1 NP
# P1 -> 'into'
# PP2 -> P2 NP
# P2 -> 'with'
# V1 -> 'loaded' | 'filled'
# V2 -> 'dumped'

# 13
# Morphological paradigms are rarely completely regular, in the sense of every cell in the matrix having a different 
# realization. For example, the present tense conjugation of the lexeme walk only has two distinct forms: walks for the 
# 3rd person singular, and walk for all other combinations of person and number. A successful analysis should not require 
# redundantly specifying that 5 out of the 6 possible morphological combinations have the same realization. Propose and 
# implement a method for dealing with this.

# for each verb, we can separate the base form from the alternative forms
# if we do this, we only need to specify the number and person for the alternative form
# for the provided example, it would look like this:
# S -> NP[AGR=?n] VP[AGR=?n]
# VP[AGR=?n] -> V[AGR=?n]
# V[AGR=[NUM=sg, PER=3]] -> 'walk'
# V -> 'walks'

# 14
# So-called head features are shared between the parent node and head child. For example, TENSE is a head feature that is 
# shared between a VP and its head V child. See (Gazdar, Klein, & and, 1985) for more details. Most of the features we have 
# looked at are head features - exceptions are SUBCAT and SLASH. Since the sharing of head features is predictable, it 
# should not need to be stated explicitly in the grammar productions. Develop an approach that automatically accounts for 
# this regular behavior of head features.

# If the head features are regular, we should just separate them into different productions.  This is the only way
# to not specify the head features in the productions themselves.

# S -> NP[AGR=[NUM=sg, PER=1]] VP11
# S -> NP[AGR=[NUM=sg, PER=3]] VP13
# ...
# VP11 -> V11
# V11 -> 'walk'
# VP13 -> V13
# V13 -> 'walks'
# ...

# 15
# Extend NLTK's treatment of feature structures to allow unification into list-valued features, and use this to implement 
# an HPSG-style analysis of subcategorization, whereby the SUBCAT of a head category is the concatenation its complements' 
# categories with the SUBCAT value of its immediate parent.

# NLTK already has a feature structure specifically for list features called FeatList.  It's documentation is here:
# http://www.nltk.org/_modules/nltk/featstruct.html
# When constructing a FeatStruct, if the structures are in a list, it automatically is converted to a FeatList.
# This type of analysis would be done on the productions themselves, so when a production was converted into the 
# FeatList, we would check that the categorization fits the specification above:

# I will be extending the FeatStructReader class
class FeatStructReader2(nltk.featstruct.FeatStructReader):
    # Redefine the function that reads the list in so we can check that it fulfils the requirements
    def _read_partial_featlist(self, s, position, match,
                                    reentrances, fstruct):
            if match.group(2): raise ValueError('open bracket')
            if not match.group(3): raise ValueError('open bracket')

            while position < len(s):
                match = self._END_FSTRUCT_RE.match(s, position)
                if match is not None:
                    # unify the parent category with the child category and the sibling
                    fstruct = fstruct.unify(featStructReader)
                    return fstruct, match.end()

                # Reentances have the form "-> (target)"
                match = self._REENTRANCE_RE.match(s, position)
                if match:
                    position = match.end()
                    match = self._TARGET_RE.match(s, position)
                    if not match: raise ValueError('identifier', position)
                    target = match.group(1)
                    if target not in reentrances:
                        raise ValueError('bound identifier', position)
                    position = match.end()
                    fstruct.append(reentrances[target])

                # Anything else is a value.
                else:
                    value, position = (
                        self._read_value(0, s, position, reentrances))
                    fstruct.append(value)

                # If there's a close bracket, handle it at the top of the loop.
                if self._END_FSTRUCT_RE.match(s, position):
                    continue

                # Otherwise, there should be a comma
                match = self._COMMA_RE.match(s, position)
                if match is None: raise ValueError('comma', position)
                position = match.end()

            # We never saw a close bracket.
            raise ValueError('close bracket', position)


# 16
# Extend NLTK's treatment of feature structures to allow productions with underspecified categories, such as 
# S[-INV] --> ?x S/?x.

# Feature structures do allow productions with underspecified categories:
    # fs1 = nltk.FeatStruct("[A = ?x, B= [C = ?x]]")
    # fs8 = nltk.FeatStruct("[A = (1)[D = ?x, G = ?x], C = [B = ?x, E -> (1)] ]")
# These examples were provided earlier in the chapter.  The way to use these would be something like this:
# nltk.FeatStruct("[A = (1)?x], B -> (1) D")

# 17
# Extend NLTK's treatment of feature structures to allow typed feature structures.
# To use this, I would first need to build a dictionary with the types as keys and lists as values.  The dictionary
# will define what values are allowed to be used for each type.  For example, if we have (person, [1, 2, 3]) in the
# dictionary, that means that any arguments named "person" are only allowed to have values 1, 2, and 3.  This prevents
# any other values outside the specified values because we will be sure to check them as they are added.

# To check that the values are allowed, this will happen when we build the feature struct, like below:

# I am extending the Feature class
class Feature2(nltk.featstruct.Feature):

    def __init__(self, name, constraints, default=None, display=None):
        assert display in (None, 'prefix', 'slash')

        self._name = name # [xx] rename to .identifier?
        self._default = default # [xx] not implemented yet.
        self._display = display
        self.constraints = constraints  # save the constraints here in a list

        if self._display == 'prefix':
            self._sortkey = (-1, self._name)
        elif self._display == 'slash':
            self._sortkey = (1, self._name)
        else:
            self._sortkey = (0, self._name)

        # check if the name is okay
        if (name not in constraints):
            raise ValueError


# 18
# This link is broken and I cannot find the article online.  All the work would be encapsulated in creating
# a set of productions with feature sets.  This is very similar to what was done in most of the earlier problems.


