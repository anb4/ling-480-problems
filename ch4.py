# NLTK Textbook Problems Chapter 4
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk, re
from copy import deepcopy
from collections import defaultdict
from operator import itemgetter
import textwrap
# import networkx as nx
import matplotlib

# 1 -> no code

# 2
# Identify three operations that can be performed on both tuples and lists. Identify three list operations 
# that cannot be performed on tuples. Name a context where using a list instead of a tuple generates a Python error.
# both: slicing, indexing, len()
# only lists: changing values, reverse, extend
# error would be generated if you called tuple.reverse()

# 3
# Find out how to create a tuple consisting of a single item. There are at least two ways to do this.
# tuple1 = (1)
# tuple2 = tuple([1])

# 4
# Create a list words = ['is', 'NLP', 'fun', '?']. Use a series of assignment statements 
# (e.g. words[1] = words[2]) and a temporary variable tmp to transform this list into the list 
#  ['NLP', 'is', 'fun', '!']. Now do the same transformation using tuple assignment.
def prob_4():
	words = ['is', 'NLP', 'fun', '?']
	temp = words[1]
	words[1] = words[0]
	words[0] = temp
	words[-1] = "!"
	print words
	words2 = ['is', 'NLP', 'fun', '?']
	tup1 = tuple(words2[1])
	words2[1] = words2[0]
	words2[0] = ''.join(list(tup1))
	words2[-1] = "!"
	print words2
# prob_4()

# 5
# Read about the built-in comparison function cmp, by typing help(cmp). How does it differ in behavior 
# from the comparison operators?
# help(cmp) returns a negative number if x < y, zero if equal, and positive if x > y rather than booleans.

# 6
# Does the method for creating a sliding window of n-grams behave correctly for the two limiting cases: 
# n = 1, and n = len(sent)?
# yes, these are edge cases but they still are correct.

# 7
# We pointed out that when empty strings and empty lists occur in the condition part of an if clause, 
# they evaluate to False. In this case, they are said to be occurring in a Boolean context. Experiment 
# with different kind of non-Boolean expressions in Boolean contexts, and see whether they evaluate 
# as True or False.
# if (1):
# 	print True
# if (0):
# 	print True

# 8
# Use the inequality operators to compare strings, e.g. 'Monty' < 'Python'. What happens when you do 
# 'Z' < 'a'? Try pairs of strings which have a common prefix, e.g. 'Monty' < 'Montague'. Read up on 
# "lexicographical sort" in order to understand what is going on here. Try comparing structured objects, 
# e.g. ('Monty', 1) < ('Monty', 2). Does this behave as expected?
# This does alphabetical comparison.  The example behaves as expected because each index gets compared.
# print (('Monty', 1) < ('Monty', 2))

# 9
# Write code that removes whitespace at the beginning and end of a string, and normalizes whitespace 
# between words to be a single space character.

# do this task using split() and join()
# do this task using regular expression substitutions
def prob_9a(text):
	words = [word for word in text.split(' ') if word != '']
	return ' '.join(words)

def prob_9b(text):
	pattern = re.compile(r'\s+')
	return pattern.sub(' ', text)

# print prob_9a('is    NLP fun?  ')
# print prob_9b('is    NLP fun?  ')


# 10
# Write a program to sort words by length. Define a helper function cmp_len which uses the cmp 
# comparison function on word lengths.
def cmp_len(word1, word2):
	return cmp(len(word1), len(word2))

def prob_10(words):
	words.sort(cmp=cmp_len)
	return words
# print prob_10(['is', 'NLP', 'fun', '?'])

# 11
# Create a list of words and store it in a variable sent1. Now assign sent2 = sent1. Modify one of the 
# items in sent1 and verify that sent2 has changed.

# Now try the same exercise but instead assign sent2 = sent1[:]. Modify sent1 again and see what happens 
# to sent2. Explain.
# Now define text1 to be a list of lists of strings (e.g. to represent a text consisting of multiple 
# 	sentences. Now assign text2 = text1[:], assign a new value to one of the words, e.g. text1[1][1]
# 	 = 'Monty'. Check what this did to text2. Explain.
# Load Python's deepcopy() function (i.e. from copy import deepcopy), consult its documentation, and
#  test that it makes a fresh copy of any object.

def prob_11():
	sent1 = ['is', 'NLP', 'fun', '?']
	sent2 = sent1
	sent1[0] = 'hi'
	print sent2
	text1 = [["hello", "world"], ["hi", "hello"]]
	text2 = text1[:]
	text1[1][1] = "Monty"
	print text2
	# this does change text2.  Deepcopy won't have the object references anymore.
	text3 = [["hello", "world"], ["hi", "hello"]]
	text4 = deepcopy(text3)
	text3[1][1] = "Monty"
	print text4
# prob_11()

# 12
 # Initialize an n-by-m list of lists of empty strings using list multiplication, e.g. 
 # word_table = [[''] * n] * m. What happens when you set one of its values, e.g. 
 # word_table[1][2] = "hello"? Explain why this happens. Now write an expression using 
 # range() to construct a list of lists, and show that it does not have this problem.

def prob_12():
	word_table = [[''] * 2] * 3
	word_table[0][1] = 'hello'
	print word_table      # [['', 'hello'], ['', 'hello'], ['', 'hello']]
	word_table2 = [['', ''] for x in range(3)]
	word_table2[0][1] = 'hello' # [['', 'hello'], ['', ''], ['', '']]
	print word_table2

# prob_12()

# 13
# Write code to initialize a two-dimensional array of sets called word_vowels and process a list of 
# words, adding each word to word_vowels[l][v] where l is the length of the word and v is the number of 
# vowels it contains.

def prob_13(words, n, m):
	word_vowels = [[set() for i in range(n)] for j in range(m)]
	for word in words:
		vowel_count = 0
		for letter in word:
			if letter in 'aeiou':
				vowel_count += 1
		word_vowels[len(word)][vowel_count] = word
	return word_vowels

# print prob_13(['is', 'NLP', 'fun', '?'], 4, 4)

# 14
# Write a function novel10(text) that prints any word that appeared in the last 10% of a text that 
# had not been encountered earlier.

def novel10(text):
	num_words = len(text)
	for word in text[int(num_words*0.9):]:
		if word not in text[:int(num_words * 0.9)]:
			print word
# novel10(['is', 'NLP', 'fun', '?', 'is', 'is', 'NLP', 'NLP', 'hello', 'hi'])

# 15
# Write a program that takes a sentence expressed as a single string, splits it and counts up the 
# words. Get it to print out each word and the word's frequency, one per line, in alphabetical order.

def prob_15(sentence):
	word_list = sentence.split(' ')
	word_freq = defaultdict(int)
	for word in word_list:
		word_freq[word] += 1
	for word in sorted(word_freq.keys()):
		print word + " " + str(word_freq[word])

# prob_15("Hello world Hello hi")

# 16
# Write a function gematria() that sums the numerical values of the letters of a word, according 
# to the letter values in letter_vals:
# Process a corpus (e.g. nltk.corpus.state_union) and for each document, count how many of its words 
# have the number 666.
# Write a function decode() to process a text, randomly replacing words with their Gematria equivalents, 
# in order to discover the "hidden meaning" of the text.

def gematria(word):
	letter_vals = {'a':1, 'b':2, 'c':3, 'd':4, 'e':5, 'f':80, 'g':3, 'h':8,'i':10, 'j':10, 'k':20, 'l':30, 'm':40, 'n':50, 'o':70, 'p':80, 'q':100, 'r':200, 's':300, 't':400, 'u':6, 'v':6, 'w':800, 'x':60, 'y':10, 'z':7}
	letter_sum = 0
	for letter in word:
		letter_sum += letter_vals[letter]
	return letter_sum

def proc_corpus():
	num_666 = 0
	for fileid in nltk.corpus.state_union.fileids():
		for word in nltk.corpus.state_union.words(fileid):
			if (gematria(word) == 666):
				num_666 += 1

def decode(text):
	for x in range(len(text)):
		if (x % 10 == 0):
			text[x] = gematria(text[x])

# 17
# Write a function shorten(text, n) to process a text, omitting the n most frequently occurring words 
# of the text. How readable is it?

def shorten(text, n):
	word_freq = defaultdict(int)
	for word in text:
		word_freq[word] += 1
	for x in range(n):
		key_to_delete = max(word_freq, key=lambda k: word_freq[k])
		del word_freq[key_to_delete]
	new_text = []
	for word in text:
		if word not in word_freq.keys():
			new_text.append(word)
	return new_text

# 18
# Write code to print out an index for a lexicon, allowing someone to look up words according to their 
# meanings (or pronunciations; whatever properties are contained in lexical entries).

def print_lexicon(lexicon):
	index = []
	for word in lexicon:
		synsets = nltk.corpus.wordnet.synsets(word)
		definitions = []
		for synset in synsets:
			definitions.append(synset.definition())
		index.append((word, definitions))
	return index


# print print_lexicon(['here', 'are', 'some', 'words', 'in', 'the', 'lexicon'])

# 19
# Write a list comprehension that sorts a list of WordNet synsets for proximity to a given synset. For 
# example, given the synsets minke_whale.n.01, orca.n.01, novel.n.01, and tortoise.n.01, sort them 
# according to their shortest_path_distance() from right_whale.n.01.

def synset_dist(synsets, test_synset):
	# result = sorted((trans for trans in my_list if trans.type in types), key=lambda x: x.code)
	return sorted((synset for synset in synsets), key=lambda x: test_synset.shortest_path_distance(x))

def do_dist():
	test_synset = nltk.corpus.wordnet.synsets("whale")[0]
	synsets = []
	synsets.append(nltk.corpus.wordnet.synsets("orca")[0])
	synsets.append(nltk.corpus.wordnet.synsets("dog")[0])
	synsets.append(nltk.corpus.wordnet.synsets("tortoise")[0])
	synsets.append(nltk.corpus.wordnet.synsets("novel")[0])
	print synset_dist(synsets, test_synset)

# do_dist()

# 20
# Write a function that takes a list of words (containing duplicates) and returns a list of words 
# (with no duplicates) sorted by decreasing frequency. E.g. if the input list contained 10 instances 
# of the word table and 9 instances of the word chair, then table would appear before chair in the output 
# list.

def freq_duplicates(words):
	freq = defaultdict(int)
	for word in words:
		freq[word] += 1
	unique_words = []
	for w in sorted(freq, key=freq.get, reverse=True):
  		unique_words.append(w)
  	return unique_words

# print freq_duplicates(['hi', 'hello', 'hi', 'world', 'world', 'world'])

# 21
# Write a function that takes a text and a vocabulary as its arguments and returns the set of words that 
# appear in the text but not in the vocabulary. Both arguments can be represented as lists of strings. 
# Can you do this in a single line, using set.difference()?

def word_diff(text, vocab):
	return set(text).difference(vocab)

# print word_diff(['hello', 'world'], ['hello'])

# 22
# Import the itemgetter() function from the operator module in Python's standard library (i.e. from 
# 	operator import itemgetter). Create a list words containing several words. Now try calling: 
# sorted(words, key=itemgetter(1)), and sorted(words, key=itemgetter(-1)).

def test_itemgetter():
	words = ['here', 'are', 'some', 'words', 'in', 'the', 'lexicon']
	print sorted(words, key=itemgetter(1))
	print sorted(words, key=itemgetter(-1))

# test_itemgetter()

# 23
# Write a recursive function lookup(trie, key) that looks up a key in a trie, and returns the value it 
# finds. Extend the function to return a word when it is uniquely determined by its prefix (e.g. vanguard
#  is the only word that starts with vang-, so lookup(trie, 'vang') should return the same thing as 
#  lookup(trie, 'vanguard')).

def lookup(trie, key):
	if ("value" in trie.keys() or key == ''):
		print trie["value"]
	else:
		trie = trie[key[0]]
		lookup(trie, key[1:])
def insert(trie, key, value):
    if key:
        first, rest = key[0], key[1:]
        if first not in trie:
            trie[first] = {}
        insert(trie[first], rest, value)
    else:
        trie['value'] = value

def run_lookup():
	trie = {}
	insert(trie, 'chat', 'cat')
	insert(trie, 'chien', 'dog')
	insert(trie, 'chair', 'flesh')
	insert(trie, 'chic', 'stylish')

	lookup(trie, 'chat')

# run_lookup()

# 24 --> link is broken.

# 25
# Read about string edit distance and the Levenshtein Algorithm. Try the implementation provided in 
# nltk.edit_distance(). In what way is this using dynamic programming? Does it use the bottom-up or 
# top-down approach? 

# It calculates the edit distance one entry at a time, based on the answers from entries previously
# computed.  This is a bottom up approach because the entries are filled starting with (0,0)

# 26
# The Catalan numbers arise in many applications of combinatorial mathematics, including the counting 
# of parse trees (6). The series can be defined as follows: C0 = 1, and Cn+1 = sum0..n (CiCn-i).

# Write a recursive function to compute nth Catalan number Cn.
# Now write another function that does this computation using dynamic programming.
# Use the timeit module to compare the performance of these functions as n increases.

def catalan_rec(n):
	if (n == 0):
		return 1
	cat_sum = 0
	for i in range(n):
		cat_sum += catalan_rec(i) * catalan_rec(n-i-1)
	return cat_sum
# print catalan_rec(2)
# gets exponentially slower

def catalan_dp(n):
	table = [0] * (n+1)
	table[0] = 1
	for i in range(n+1):
		for j in range(i):
			table[i] += table[j] * table[i-j-1]
	return table[n]

# print catalan_dp(4)
# execution time grows at a constant rate

# 27 -> link is broken

# 28 -> link is broken

# 29
# Write a recursive function that pretty prints a trie in alphabetically sorted order, e.g.:
# chair: 'flesh'
# ---t: 'cat'
# --ic: 'stylish'
# ---en: 'dog'

def print_trie(trie):
	if ("value" in trie.keys()):
	 	sys.stdout.write(": " + trie["value"] + "\n")
	 	return
	index = 0
	for key in sorted(trie.keys()):
		sys.stdout.write("-" * index)
	 	sys.stdout.write(key)
	 	print_trie(trie[key])
	 	index += 1
	return


def make_trie():
	trie = {}
	insert(trie, 'chat', 'cat')
	insert(trie, 'chien', 'dog')
	insert(trie, 'chair', 'flesh')
	insert(trie, 'chic', 'stylish')
	return trie

# trie = make_trie()
# print_trie(trie)

# 30
# With the help of the trie data structure, write a recursive function that processes 
# text, locating the uniqueness point in each word, and discarding the remainder of 
# each word. How much compression does this give? How readable is the resulting text?

def lookup2(trie, key):
	if ("value" in trie.keys() or key == ''):
		print key, trie["value"]
	else:
		trie = trie[key[0]]
		lookup(trie, key[1:])

def run_lookup2():
	trie = {}
	insert(trie, 'chat', 'cat')
	insert(trie, 'chien', 'dog')
	insert(trie, 'chair', 'flesh')
	insert(trie, 'chic', 'stylish')
	text = ['chat', 'chien', 'chair', 'chic']
	for word in text:
		lookup2(trie, word)

# run_lookup2()

# 31
# Obtain some raw text, in the form of a single, long string. Use Python's textwrap 
# module to break it up into multiple lines. Now write code to add extra spaces between 
# words, in order to justify the output. Each line must have the same width, and spaces 
# must be approximately evenly distributed across each line. No line can begin or end 
# with a space.

def text_wrap(text):
	dedented_text = textwrap.dedent(text).strip()
	print(textwrap.fill(dedented_text, width=30))

# text_wrap('this is a long string that has many words and goes on for a long time but it will be formatted nicely so that the width is even and the edges are aligned.')

# 32
# Develop a simple extractive summarization tool, that prints the sentences of a document
#  which contain the highest total word frequency. Use FreqDist() to count word 
#  frequencies, and use sum to sum the frequencies of the words in each sentence. Rank 
#  the sentences according to their score. Finally, print the n highest-scoring sentences 
#  in document order. Carefully review the design of your program, especially your 
#  approach to this double sorting. Make sure the program is written as clearly as 
#  possible.

def extractive_summarization(n):
	fd = nltk.FreqDist(nltk.corpus.brown.words(categories='lore'))
	sentence_scores = {}
	for sentence in nltk.corpus.brown.sents(categories='lore'):
		freq_sum = 0
		for word in sentence:
			freq_sum += fd.freq(word)
		sentence_scores[' '.join(sentence)] = freq_sum
	index = 0
	for sentence in sorted(sentence_scores, key=sentence_scores.get):
		if (index > n):
			break
		else:
			print sentence
			index += 1

# extractive_summarization(2)

# 33
# Read the following article on semantic orientation of adjectives. Use the NetworkX 
# package to visualize a network of adjectives with edges to indicate same vs different 
# semantic orientation.

positive = ['hot', 'high', 'best']
negative = ['cold', 'low', 'worst']

def traverse_positive(graph, start, node):
    graph.depth[node] = 3
    for synonym in positive:
        graph.add_edge(node, synonym)
        traverse(graph, start, synonym)

def make_graph():
    G = nx.Graph()
    G.depth = {}
    traverse_positive(G, 'hot', 'hot')  # creates edges between positive adjs
    traverse_positive(G, 'cold', 'cold') # creates edges between negative adjs
    return G

def graph_draw(graph):
    nx.draw_graphviz(graph,
         node_size = [16 * graph.degree(n) for n in graph],
         node_color = [graph.depth[n] for n in graph],
         with_labels = False)
    matplotlib.pyplot.show()


# 34 -> link is broken

# 35
def find_word_squares(n, matrix):
	for i in range(n):
		match = 1
		for j in range(n):
			if (matrix[i][j] != matrix[j][i]):
				match = 0
				break
		if (match == 1):
			print "match found at n = " + str(i)

# just tested with numbers for easy debugging.  these can be replaced with letters.
# find_word_squares(3, [[1, 2, 3], 
					  # [2, 5, 9], 
					  # [3, 2, 4]])
