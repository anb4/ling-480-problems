# NLP Chapter 8 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
import nltk.tree
from collections import defaultdict
from timeit import Timer
import time
import re
from random import randint
from nltk.parse.generate import generate


# 1
# Can you come up with grammatical sentences that have probably never been uttered before? (Take turns with a partner.) 
# What does this tell you about human language?

# "I washed the keyboard on the back deck while I was wearing a shirt as flip flops."
# This tells us that human language is generative, meaning that it is guided by grammatical rules, but an infinite
# number of utterances can be created within the bounds of those rules.

# 2
# Recall Strunk and White's prohibition against sentence-initial however used to mean "although". Do a web search for 
# however used at the start of the sentence. How widely used is this construction?

# This construction is very widely used.  Most sites I looked at said that it doesn't make sense to use the Strunk
# White rule anymore because of how widely used this construction is.

# 3
# Consider the sentence Kim arrived or Dana left and everyone cheered. Write down the parenthesized forms to show the 
# relative scope of and and or. Generate tree structures corresponding to both of these interpretations.

# (Kim arrived) or (Dana left and everyone cheered).
# (Kim arrived or Dana left) and (everyone cheered).
# The tree structure here depends on which S you parse first.  This depends on whether the or/and are parsed before the other one.
# This function produces both trees.

def prob_3():
	grammar = nltk.CFG.fromstring("""
		S -> S C S
		S -> NP VP
		VP -> V
		NP -> N
		V -> "arrived" | "left" | "cheered"
		N -> "Kim" | "Dana" | "everyone"
		C -> "or" | "and"
		""")
	sent = ["Kim", "arrived", "or", "Dana", "left", "and", "everyone", "cheered"]
	parser = nltk.ChartParser(grammar)
	for tree in parser.parse(sent):
		print(tree)

# prob_3()

# 4
# The Tree class implements a variety of other useful methods. See the Tree help documentation for more details, 
# i.e. import the Tree class and then type help(Tree).

def prob_4():
	help(nltk.tree.Tree)

# prob_4()

# # 5
# In this exercise you will manually construct some parse trees.
# Write code to produce two trees, one for each reading of the phrase old men and women
# Encode any of the trees presented in this chapter as a labeled bracketing and use nltk.Tree() to check that it is 
# well-formed. Now use draw() to display the tree.
# As in (a) above, draw a tree for The woman saw a man last Thursday.

def prob_5():
	grammar = nltk.CFG.fromstring("""
	NP -> NP C NP
	NP -> N
	NP -> ADJ NP
	ADJ -> "old"
	N -> "men" | "women"
	C -> "and"
	""")
	sent = ["old", "men", "and", "women"]
	parser = nltk.ChartParser(grammar)
	for tree in parser.parse(sent):
		print(tree)
	
	tree = nltk.tree.Tree("S", [nltk.tree.Tree("NP", ["Mary"]), nltk.tree.Tree("VP", [nltk.tree.Tree("V", ["saw"]), nltk.tree.Tree("NP", [nltk.tree.Tree("DET", ["a"]), nltk.tree.Tree("N", ["dog"])])])])
	# must comment this out to get the last part to run.  Can uncomment to see the tree.
	# tree.draw()

	grammar2 = nltk.CFG.fromstring("""
	S -> NP VP
	VP -> V NP
	NP -> DET NP
	NP -> N
	NP -> NP ADVP
	ADVP -> ADV NP
	ADV -> "last"
	V -> "saw"
	N -> "man" | "woman" | "Thursday"
	DET -> "the" | "The"
	""")

	sent1 = ["The", "woman", "saw", "the", "man", "last", "Thursday"]
	parser = nltk.ChartParser(grammar2)
	for tree in parser.parse(sent1):
		print(tree)

# prob_5()

# 6
# Write a recursive function to traverse a tree and return the depth of the tree, such that a tree with a single node
# would have depth zero. (Hint: the depth of a subtree is the maximum depth of its children, plus one.)

def prob_6(tree):
	children = [child for child in tree]
	if len(children) == 1:
		return 0
	else:
		depths = []
		for child in children:
			depths.append(prob_6(child))
		return max(depths) + 1

def run_6():
	tree = nltk.tree.Tree("S", [nltk.tree.Tree("NP", ["Mary"]), nltk.tree.Tree("VP", [nltk.tree.Tree("V", ["saw"]), nltk.tree.Tree("NP", [nltk.tree.Tree("DET", ["a"]), nltk.tree.Tree("N", ["dog"])])])])
	print prob_6(tree)

# run_6()

# 7
# Analyze the A.A. Milne sentence about Piglet, by underlining all of the sentences it contains then replacing these with 
# S (e.g. the first sentence becomes S when:lx` S). Draw a tree structure for this "compressed" sentence. What are the 
# main syntactic constructions used for building such a long sentence?

# In after-years S that S, but S, when S, and the story went on and on, rather like this sentence, until S, at which moment, luckily,...
# "that", "but", "when", "until", "at which moment"

# 8
# In the recursive descent parser demo, experiment with changing the sentence to be parsed by selecting Edit Text in 
# the Edit menu.

# nltk.app.rdparser()

# 9
# Can the grammar in grammar1 be used to describe sentences that are more than 20 words in length?

# grammar1 = nltk.CFG.fromstring("""
#   S -> NP VP
#   VP -> V NP | V NP PP
#   PP -> P NP
#   V -> "saw" | "ate" | "walked"
#   NP -> "John" | "Mary" | "Bob" | Det N | Det N PP
#   Det -> "a" | "an" | "the" | "my"
#   N -> "man" | "dog" | "cat" | "telescope" | "park"
#   P -> "in" | "on" | "by" | "with"
#   """)

# DET N P DET N P NP ... VP
# you can keep expaning the PP from NP -> Det N PP over and over forever.

# 10
# Use the graphical chart-parser interface to experiment with different rule invocation strategies. Come up with your 
# own strategy that you can execute manually using the graphical interface. Describe the steps, and report any efficiency 
# improvements it has (e.g. in terms of the size of the resulting chart). Do these improvements depend on the structure 
# of the grammar? What do you think of the prospects for significant performance boosts from cleverer rule invocation 
# strategies?

# I think the best strategy here is bottom up.  If you identify what category the words need to be under, you can make
# predictions about decisions further up the tree.  The size of the resulting chart should be as small as possible
# because you aren't making decisions that won't lead you to a correct parse.  It does depend on the structure of the
# grammar to be able to look forward and decide which choices would help most later on.  I do think that using the left
# corner strategy defined in the chapter could provide a performance boost compared to this strategy.

# 11
# With pen and paper, manually trace the execution of a recursive descent parser and a shift-reduce parser, for a CFG 
# you have already seen, or one of your own devising.

# done

# 12
# We have seen that a chart parser adds but never removes edges from a chart. Why?

# It makes the optimal decision at every point, so it never needs to go back and remove edges.  This is the benefit
# of dynamic programming.

# 13
# Consider the sequence of words: Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo. This is a grammatically 
# correct sentence, as explained at 
# http://en.wikipedia.org/wiki/Buffalo_buffalo_Buffalo_buffalo_buffalo_buffalo_Buffalo_buffalo. 
# Consider the tree diagram presented on this Wikipedia page, and write down a suitable grammar. Normalize case to 
# lowercase, to simulate the problem that a listener has when hearing this sentence. Can you find other parses for 
# this sentence? How does the number of parse trees grow as the sentence gets longer? (More examples of these sentences 
# can be found at http://en.wikipedia.org/wiki/List_of_homophonous_phrases).

def prob_13():
	sent = ["buffalo", "buffalo", "buffalo", "buffalo", "buffalo", "buffalo", "buffalo", "buffalo"]
	grammar = nltk.CFG.fromstring("""
	  S -> NP VP
	  NP -> NP RC | PN N
	  RC -> NP V
	  VP -> V NP
	  V -> "buffalo"
	  PN -> "buffalo"
	  N -> "buffalo"
	  """)
	parser = nltk.ChartParser(grammar)
	for tree in parser.parse(sent):
		print(tree)

# This parser produces 2 parse trees for the buffalo sentence.  The number of parse trees should grow exponentially
# as the sentence gets longer.
# prob_13()

# 14
# You can modify the grammar in the recursive descent parser demo by selecting Edit Grammar in the Edit menu. 
# Change the second expansion production, namely NP -> Det N PP, to NP -> NP PP. Using the Step button, try to build 
# a parse tree. What happens?

# nltk.app.rdparser()
# It's impossible to use that production because it is right-recursive.  This means that you could use that rule
# forever because you can just keep replacing the NP with NP PP.

# 15
# Extend the grammar in grammar2 with productions that expand prepositions as intransitive, transitive and requiring a 
# PP complement. Based on these productions, use the method of the preceding exercise to draw a tree for the sentence 
# Lee ran away home.

def prob_15():
	grammar2 = nltk.CFG.fromstring("""
	  S  -> NP VP
	  NP -> Det Nom | PropN | Nom
	  Nom -> Adj Nom | N
	  VP -> V Adj | V NP | V S | V NP PP | V PP
	  PP -> P NP | NP P NP
	  PropN -> 'Buster' | 'Chatterer' | 'Joe'
	  Det -> 'the' | 'a'
	  N -> 'bear' | 'squirrel' | 'tree' | 'fish' | 'log' | "Lee" | "home"
	  Adj  -> 'angry' | 'frightened' |  'little' | 'tall'
	  V ->  'chased'  | 'saw' | 'said' | 'thought' | 'was' | 'put' | "ran"
	  P -> 'on' | "away"
  	""")

  	sent = ["Lee", "ran", "away", "home"]
	parser = nltk.ChartParser(grammar2)
	for tree in parser.parse(sent):
		print(tree)
	# (S (NP (Nom (N Lee))) (VP (V ran) (PP (P away) (NP (Nom (N home))))))

# prob_15()

# 16
# Pick some common verbs and complete the following tasks:
# Write a program to find those verbs in the Prepositional Phrase Attachment Corpus nltk.corpus.ppattach. Find any cases 
# where the same verb exhibits two different attachments, but where the first noun, or second noun, or preposition, stay 
# unchanged (as we saw in our discussion of syntactic ambiguity in 2).
# Devise CFG grammar productions to cover some of these cases.

def prob_16():
	entries = nltk.corpus.ppattach.attachments('training')
	table = defaultdict(lambda: defaultdict(set))
	for entry in entries:
		key = entry.noun1 + '-' + entry.prep + '-' + entry.noun2
		table[key][entry.verb].add(entry.attachment)
	for key in sorted(table):
		for verb in sorted(table[key]):
			if (len(table[key][verb]) > 1):
				print table[key][verb]
	# these are all either V or N.  So, the grammar would look like this:

	# grammar2 = nltk.CFG.fromstring("""
	#   ...
	#   VP -> V N | V V
	# ...
 #  	""")
	# This makes sense because a lot of these cases are verbs that can be used as helping verbs.

# prob_16()

# 17
# Write a program to compare the efficiency of a top-down chart parser compared with a recursive descent parser (4). Use 
# the same grammar and input sentences for both. Compare their performance using the timeit module (see 4.7 for an example 
# 	of how to do this).

def prob_17():
	grammar2 = nltk.CFG.fromstring("""
	  S  -> NP VP
	  NP -> Det Nom | PropN | Nom
	  Nom -> Adj Nom | N
	  VP -> V Adj | V NP | V S | V NP PP | V PP
	  PP -> P NP | NP P NP
	  PropN -> 'Buster' | 'Chatterer' | 'Joe'
	  Det -> 'the' | 'a'
	  N -> 'bear' | 'squirrel' | 'tree' | 'fish' | 'log' | "Lee" | "home"
	  Adj  -> 'angry' | 'frightened' |  'little' | 'tall'
	  V ->  'chased'  | 'saw' | 'said' | 'thought' | 'was' | 'put' | "ran"
	  P -> 'on' | "away"
  	""")

  	sent = ["Lee", "ran", "away", "home"]
  	chart_parser = nltk.ChartParser(grammar2)
  	start_chart = time.time()
  	chart_parser.parse(sent)
  	end_chart = time.time()
  	print(end_chart - start_chart)
  	# 0.000927925109863 seconds

  	recursive_parser = nltk.RecursiveDescentParser(grammar2)
  	start_recursive = time.time()
  	recursive_parser.parse(sent)
  	end_recursive = time.time()
  	print(end_recursive - start_recursive)
  	# 1.28746032715e-05 seconds
	
# prob_17()

# 18
# Compare the performance of the top-down, bottom-up, and left-corner parsers using the same grammar and three 
# grammatical test sentences. Use timeit to log the amount of time each parser takes on the same sentence. Write a 
# function that runs all three parsers on all three sentences, and prints a 3-by-3 grid of times, as well as row and 
# column totals. Discuss your findings.

def run_it(parser, sent):
	start = time.time()
	parser.parse(sent)
	end = time.time()
	return (end - start)

def run_round(chart_parser, recursive_parser, left_corner_parser, sent, trial_num):
	chart_time = run_it(chart_parser, sent)
	recursive_time = run_it(recursive_parser, sent)
	left_time = run_it(left_corner_parser, sent)
	total = chart_time + recursive_time + left_time
	print str(trial_num) + "       " + str(chart_time) + "       " + str(recursive_time) + "       " + str(left_time) + "       " + str(total)
	return [chart_time, recursive_time, left_time, total]

def prob_18():
	grammar2 = nltk.CFG.fromstring("""
		S -> NP VP
		VP -> V NP | V
		NP -> DET NP
		NP -> N
		NP -> NP ADVP
		ADVP -> ADV NP
		ADV -> "last"
		V -> "saw" | "ran"
		N -> "man" | "woman" | "Thursday"
		DET -> "the" | "The"
		""")
  	
  	chart_parser = nltk.ChartParser(grammar2)
  	recursive_parser = nltk.RecursiveDescentParser(grammar2)
  	left_corner_parser = nltk.parse.BottomUpLeftCornerChartParser(grammar2)

	print("           Top-down:           Bottom-up:              Left-corner:         Sent total:")
	sent1 = ["The", "woman", "saw", "the", "man", "last", "Thursday"]
	sent2 = ["The", "woman", "saw", "the", "man"]
	sent3 = ["The", "woman", "ran"]
	times1 = run_round(chart_parser, recursive_parser, left_corner_parser, sent1, 1)
	times2 = run_round(chart_parser, recursive_parser, left_corner_parser, sent2, 2)
	times3 = run_round(chart_parser, recursive_parser, left_corner_parser, sent3, 3)

	td_times = times1[0] + times2[0] + times3[0]
	rec_times = times1[1] + times2[1] + times3[1]
	left_times = times1[2] + times2[2] + times3[2]
	totals = times1[3] + times2[3] + times3[3]
	print "Total:  " + str(td_times) + "       " + str(rec_times) + "       " + str(left_times) + "       " + str(totals)

# The bottom-up parser is the fastest out of the three.  The top-down and left-corner parsers are very close.
# print prob_18()

# 19
# Read up on "garden path" sentences. How might the computational work of a parser relate to the difficulty humans have 
# with processing these sentences? http://en.wikipedia.org/wiki/Garden_path_sentence

# Typically, humans have to go back and re-process the sentence if they hit a new word that doesn't fit with the
# original parse.  This would also be true for parsers.  They could have a working parse up until that ambiguous
# point, and then have to backtrack, causing mork work to be necessary.

# 20
# To compare multiple trees in a single window, we can use the draw_trees() method. Define some trees and try it out:

def prob_20():
	tree1 = nltk.tree.Tree("S", [nltk.tree.Tree("NP", ["Mary"]), nltk.tree.Tree("VP", [nltk.tree.Tree("V", ["saw"]), nltk.tree.Tree("NP", [nltk.tree.Tree("DET", ["a"]), nltk.tree.Tree("N", ["dog"])])])])
	tree2 = nltk.tree.Tree("S", [nltk.tree.Tree("NP", ["Mary"]), nltk.tree.Tree("VP", [nltk.tree.Tree("V", ["saw"])])])

	nltk.draw.tree.draw_trees(tree1, tree2)

# prob_20()

# 21
# Using tree positions, list the subjects of the first 100 sentences in the Penn treebank; to make the results easier to 
# view, limit the extracted subjects to subtrees whose height is 2.

# prob_6() returns the depth of the tree
def prob_21():
	t = nltk.corpus.treebank.parsed_sents()[:100]
	for tree in t:
		for subtree in tree:
			# print subtree
			if (subtree.label().startswith("NP")):
				print subtree
				break

# prob_21()

# 22
# Inspect the Prepositional Phrase Attachment Corpus and try to suggest some factors that influence PP attachment.

# The factors that influences PP attachment are the meaning of the preposition, the possible noun attachment,
# the possible verb attachment, and the object of the preposition.  For example, you can have two sentences like this:
# "I ate spaghetti with meatballs" and "I ate spaghetti with a fork"
# For these sentences, the only thing distinguishing them is the meaning of the object of the preposition.
# This means that you would have to understand that meatball means food while fork means eating utensil to 
# properly categorize the attachments in these examples.

# 23
# In this section we claimed that there are linguistic regularities that cannot be described simply in terms of 
# n-grams. Consider the following sentence, particularly the position of the phrase in his turn. Does this illustrate 
# a problem for an approach based on n-grams?

# What was more, the in his turn somewhat youngish Nikolay Parfenovich also turned out to be the only person in the 
# entire world to acquire a sincere liking to our "discriminated-against" public procurator.

# This does present an issue.  When we look at the trigram "in his turn", it makes senses as a whole. However, if we
# inspect the trigrams that hold parts of this phrase ("the in his", "his turn somewhat"), these seem ungrammatical
# without further context.  Therefore, n-gram analysis can misconstrue parsing by not being able to inspect the
# broader picture.

# 24
# Write a recursive function that produces a nested bracketing for a tree, leaving out the leaf nodes, and displaying 
# the non-terminal labels after their subtrees. 

# COME BACK TO THIS ONE!!!!!

# 25
# Download several electronic books from Project Gutenberg. Write a program to scan these texts for any extremely long 
# sentences. What is the longest sentence you can find? What syntactic construction(s) are responsible for such long 
# sentences?

def prob_25():
	text = ''.join(open('gutenberg.txt').readlines())
	sentences = re.split(r' *[\.\?!][\'"\)\]]* *', text)

	sent_lens = defaultdict(list)
	for i in range(len(sentences)):
		num_words = len(sentences[i])
		sent_lens[num_words].append(i)

	# longest sentence: 691 words.  This is the copywrite disclaimer, and it uses a ton of conjunctions as well as
	# very long lists.
	print max(sent_lens)
	print sent_lens[691]
	print sentences[1900]

# prob_25()

# 26
# Modify the functions init_wfst() and complete_wfst() so that the contents of each cell in the WFST is a set of 
# non-terminal symbols rather than a single non-terminal.

def init_wfst(tokens, grammar):
    numtokens = len(tokens)
    wfst = [[set() for i in range(numtokens+1)] for j in range(numtokens+1)]
    for i in range(numtokens):
        productions = grammar.productions(rhs=tokens[i])
        wfst[i][i+1].add(productions[0].lhs())
    return wfst

def complete_wfst(wfst, tokens, grammar, trace=False):
    index = dict((p.rhs(), p.lhs()) for p in grammar.productions())
    numtokens = len(tokens)
    for span in range(2, numtokens+1):
        for start in range(numtokens+1-span):
            end = start + span
            for mid in range(start+1, end):
                nt1, nt2 = wfst[start][mid], wfst[mid][end]
                if nt1 and nt2 and (nt1,nt2) in index:
                    wfst[start][end].add(index[(nt1,nt2)])
                    if trace:
                        print("[%s] %3s [%s] %3s [%s] ==> [%s] %3s [%s]" % \
                        (start, nt1, mid, nt2, end, start, index[(nt1,nt2)], end))
    return wfst

# 27
# Consider the algorithm in 4.4. Can you explain why parsing context-free grammar is proportional to n3, where n is 
# the length of the input sentence.

# We have to fill in each entry in the matrix, which is nxn entries, or n^2.  To fill each of those entries,
# we need to do O(n) work.  This makes the parsing O(n^3)

# 28
# Process each tree of the Treebank corpus sample nltk.corpus.treebank and extract the productions with the help of 
# Tree.productions(). Discard the productions that occur only once. Productions with the same left hand side, and 
# similar right hand sides can be collapsed, resulting in an equivalent but more compact set of rules. Write code to 
# output a compact grammar.
# ????

# 29
# One common way of defining the subject of a sentence S in English is as the noun phrase that is the child of S and the 
# sibling of VP. Write a function that takes the tree for a sentence and returns the subtree corresponding to the subject 
# of the sentence. What should it do if the root node of the tree passed to this function is not S, or it lacks a subject?

def prob_29(tree):
	if tree.label() == "S":
		found_NP = False
		found_VP = False
		subtrees = [subtree for subtree in tree]
		for i in range(len(subtrees)):
			if subtrees[i].label() == "NP":
				found_NP = True
				index_NP = i
			if subtrees[i].label() == "VP":
				found_VP = True
	if (found_NP and found_VP):
		return subtrees[index_NP]
	else:
		return None

tree1 = nltk.tree.Tree("S", [nltk.tree.Tree("NP", ["Mary"]), nltk.tree.Tree("VP", [nltk.tree.Tree("V", ["saw"]), nltk.tree.Tree("NP", [nltk.tree.Tree("DET", ["a"]), nltk.tree.Tree("N", ["dog"])])])])

# print prob_29(tree1)

# 30
# Write a function that takes a grammar (such as the one defined in 3.1) and returns a random sentence generated by the 
# grammar. (Use grammar.start() to find the start symbol of the grammar; grammar.productions(lhs) to get the list of 
# 	productions from the grammar that have the specified left-hand side; and production.rhs() to get the right-hand 
# 	side of a production.)


# def make_sentence(grammar, lhs):
# 	rhs_list = grammar.productions(lhs)
# 	# for rhs in rhs_list:
# 		# print "rhs are: " + rhs
# 	print(len(rhs_list))
# 	if (len(rhs_list) == 0):
# 		return lhs
# 	index = randint(0, len(rhs_list) - 1)
# 	production = rhs_list[index]
# 	# print "production: " + production
# 	rhs = production.rhs()
# 	sentence = ""
# 	print "SYMBOLS"
# 	for symbol in rhs:
# 		print symbol
# 		# sentence += make_sentence(grammar, symbol)
# 		new = make_sentence(grammar, symbol)
# 		print new.repr()


def prob_30():
	grammar = nltk.CFG.fromstring("""
	  S -> NP VP
	  VP -> V NP | V NP PP
	  PP -> P NP
	  V -> "saw" | "ate" | "walked"
	  NP -> "John" | "Mary" | "Bob" | Det N | Det N PP
	  Det -> "a" | "an" | "the" | "my"
	  N -> "man" | "dog" | "cat" | "telescope" | "park"
	  P -> "in" | "on" | "by" | "with"
  	""")

  	len_sentences = 0

  	for sentence in generate(grammar, n=4):
  		len_sentences += 1
  	index = randint(0, len_sentences - 1)

  	check_index = 0
  	for sentence in generate(grammar, n=4):
  		if (check_index == index):
  			print(" ".join(sentence))
  			break
  		check_index += 1

# prob_30()


# Unsure on 31 through 35.






