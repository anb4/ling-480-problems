from __future__ import division  # Python 2 users only

import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")

import nltk, re, pprint
from nltk import word_tokenize
from bs4 import BeautifulSoup
import urllib
from nltk.corpus import udhr
from random import choice
import nltk.tokenize.punkt
from nltk.corpus import wordnet as wn
from collections import defaultdict


# 1
# Define a string s = 'colorless'. Write a Python statement that changes this to "colourless" 
# using only the slice and concatenation operations.

def fix_word():
	s = 'colorless'
	return s[:4] + 'u' + s[4:]
# print fix_word()

# 2
# We can use the slice notation to remove morphological endings on words. For example, 
# 'dogs'[:-1] removes the last character of dogs, leaving dog. Use slice notation to remove 
# the affixes from these words (we've inserted a hyphen to indicate the affix boundary, but 
# 	omit this from your strings): dish-es, run-ning, nation-ality, un-do, pre-heat.

def remove_affixes():
	fixed = []
	word1 = 'dishes'
	fixed.append(word1[:4])
	word2 = 'running'
	fixed.append(word2[:3])
	word3 = 'nationality'	
	fixed.append(word3[:6])
	word4 = 'undo'
	fixed.append(word4[:2])
	word5 = 'preheat'
	fixed.append(word5[3:])
	return fixed
# print remove_affixes()

# 3
# We saw how we can generate an IndexError by indexing beyond the end of a string. 
# Is it possible to construct an index that goes too far to the left, before the start of the string?

# No, because any negative index will be used as a wrap-around.

# 4
# We can specify a "step" size for the slice. The following returns every second character within 
# the slice: monty[6:11:2]. It also works in the reverse direction: monty[10:5:-2] Try these for 
# yourself, then experiment with different step values.

monty = 'this is a long string'
# print monty[6:10:2]
# print monty[10:5:-4]

# 5
# What happens if you ask the interpreter to evaluate monty[::-1]? Explain why this is a reasonable result.
# It evaluates the string in backwards order.  This is reasonable because it is requesting the entire string,
# but starting at the end and moving back.

# 6
# Describe the class of strings matched by the following regular expressions.
# [a-zA-Z]+  any sequence of alphabetical characters
# [A-Z][a-z]*   any word with only alphabetical characters that starts with a capital letter
# p[aeiou]{,2}t    p_{,2}t with any vowel in the blank
# \d+(\.\d+)?     any number with 0 or 1 decimal
# ([^aeiou][aeiou][^aeiou])*     a consonant, then a vowel, then a consonant, repeated 0 or more times
# \w+|[^\w\s]+     everything (can be a word or not a word) but whitespace



# 7
# Write regular expressions to match the following classes of strings:
# A single determiner (assume that a, an, and the are the only determiners).
# \b((an)|(a)|(the))
# An arithmetic expression using integers, addition, and multiplication, such as 2*3+8. 
# ((\d?)+[+-/*](\d)+)+

# 8
# Write a utility function that takes a URL as its argument, and returns the contents of the 
# URL, with all HTML markup removed. Use from urllib import request and then  
# request.urlopen('http://nltk.org/').read().decode('utf8') to access the contents of the URL.

def url_contents(url):
	html = urllib.request.urlopen('http://nltk.org/').read().decode('utf8')
	raw = BeautifulSoup(html).get_text()
	tokens = word_tokenize(raw)
	return tokens

# 9
# Save some text into a file corpus.txt. Define a function load(f) that reads from the file 
# named in its sole argument, and returns a string containing the text of the file.

# Use nltk.regexp_tokenize() to create a tokenizer that tokenizes the various kinds of punctuation 
# in this text. Use one multi-line regular expression, with inline comments, using the verbose flag (?x).

# Use nltk.regexp_tokenize() to create a tokenizer that tokenizes the following kinds of 
# expression: monetary amounts; dates; names of people and organizations.

def load(f):
	opened_f = open(f)
	raw = opened_f.read()
	return raw

def tokenize(f, pattern):
	text = load(f)
	nltk.regexp_tokenize(text, pattern)

punctuation_pattern = r'''(?x)
					\W
					'''
monetary_pattern = r'''(?x)
					\$?\d+(\.\d+)?%?
					'''
date_pattern = r'''(?x)
				(\d+/\d+/\d+)
				'''
names_pattern = r'''(?x)
				[A-Z][a-z]+
				'''

# 10
# Rewrite the following loop as a list comprehension:

# sent = ['The', 'dog', 'gave', 'John', 'the', 'newspaper']
# result = [(word, len(word)) for word in sent]
# print result

# 11
# Define a string raw containing a sentence of your own choosing. 
# Now, split raw on some character other than space, such as 's'.

# raw = 'This is my sentence.'
# print raw.split('s')

# 12
# Write a for loop to print out the characters of a string, one per line.
# my_string = 'Print out one by one'
# for i in range(len(my_string)):
# 	print my_string[i]

# 13
# What is the difference between calling split on a string with no argument or with ' ' as the argument, 
# e.g. sent.split() versus sent.split(' ')? What happens when the string being split contains tab 
# characters, consecutive space characters, or a sequence of tabs and spaces? (In IDLE you will 
# 	need to use '\t' to enter a tab character.)

# split() without any arguments will split on all whitespace, wherease using ' ' as an argument will
# split on each space, which doesn't include tabs or consecutive space characters.

# 14
# Create a variable words containing a list of words. Experiment with words.sort() and 
# sorted(words). What is the difference?

# words = ['this', 'is', 'a', 'list', 'of', 'words']
# print words.sort()
# print sorted(words)

# list.sort() returns None and modifies the list in place.  sorted(list) returns a sorted version.

# 15
# Explore the difference between strings and integers by typing the following at a Python prompt:
#  "3" * 7 and 3 * 7. Try converting between strings and integers using int("3") and str(3).

# 16 (working on prompt, following directions--no code to write)

# 17
# What happens when the formatting strings %6s and %-6s are used to display strings that are 
# longer than six characters?

# The first 6 characters or the last 6 characters will be taken from the string and used.

# 18
# Read in some text from a corpus, tokenize it, and print the list of all wh-word types that 
# occur. (wh-words in English are used in questions, relative clauses and exclamations: who, 
# 	which, what, and so on.) Print them in order. Are any words duplicated in this list, because 
# of the presence of case distinctions or punctuation?

def wh_word_finder(filename):
	f = open(filename)
	raw = f.read()
	tokens = word_tokenize(raw)
	matched_wh_tokens = [token for token in tokens if token.startswith('wh')]
	return matched_wh_tokens.sort()

# 19
# Create a file consisting of words and (made up) frequencies, where each line consists of a word, 
# the space character, and a positive integer, e.g. fuzzy 53. Read the file into a Python list using  
#  open(filename).readlines(). Next, break each line into its two fields using split(), and 
#  convert the number into an integer using int(). The result should be a list of the 
#  form: [['fuzzy', 53], ...].

def read_freq(filename):
	raw = open(filename).readlines()
	new_list = [[line.split(' ')[0], int(line.split(' ')[1])] for line in raw]
	return new_list


# 20
# Write code to access a favorite webpage and extract some text from it. For example, access a 
# weather site and extract the forecast top temperature for your town or city today.

def get_weather():
	url = 'http://www.accuweather.com/en/us/houston-tx/77002/weather-forecast/351197'
	html = urllib.request.urlopen(url).read().decode('utf8')
	text = BeautifulSoup(html)
	weather = text.find(class_="info city-fcast-text")
	print(weather.get_text())

# 21
# Write a function unknown() that takes a URL as its argument, and returns a list of unknown words 
# that occur on that webpage. In order to do this, extract all substrings consisting of lowercase 
# letters (using  re.findall()) and remove any items from this set that occur in the Words Corpus 
# (nltk.corpus.words). Try to categorize these words manually and discuss your findings.

def unknown(url):
	html = urllib.request.urlopen(url).read().decode('utf8')
	raw = BeautifulSoup(html)
	text = raw.get_text()
	known_words = nltk.corpus.words.words()
	pattern = r'\b[a-z]+'
	all_lc_words = re.findall(pattern, text)
	unknown_words = [word for word in all_lc_words if word not in known_words]
	return unknown_words

# 22
# Examine the results of processing the URL http://news.bbc.co.uk/ using the regular expressions 
# suggested above. You will see that there is still a fair amount of non-textual data there, 
# particularly Javascript commands. You may also find that sentence breaks have not been properly 
# preserved. Define further regular expressions that improve the extraction of text from this 
# web page.

def fixed_unknown(url):
	html = urllib.request.urlopen(url).read().decode('utf8')
	raw = BeautifulSoup(html)
	# regular expressions probably won't help here, but we can remove more data before that
	
	text = raw.get_text()
	known_words = nltk.corpus.words.words()
	pattern = r'\b[a-z]+'
	all_lc_words = re.findall(pattern, text)
	unknown_words = [word for word in all_lc_words if word not in known_words]
	return unknown_words

# 23
# Are you able to write a regular expression to tokenize text in such a way that the word 
# don't is tokenized into do and n't? Explain why this regular expression won't work: n't|\w+
# The example doesn't use an escaped apostrophe.

# pattern = r'\b(do)(n\'t)'

# 24
# Try to write code to convert text into hAck3r, using regular expressions and substitution, 
# where e = 3, i = 1, o = 0, l = |, s = 5, . = 5w33t!, ate = 8. Normalize the text to lowercase
#  before converting it. Add more substitutions of your own. Now try to map s to two different 
#  values: $ for word-initial s, and 5 for word-internal s.

def hacker_translate(filename):
	f = open(filename)
	raw = f.read()
	lwcase = raw.lower()

	pattern = re.compile(r'ate')
	text = pattern.sub('8', lwcase)

	pattern = re.compile(r'e')
	text = pattern.sub('3', text)

	pattern = re.compile(r'i')
	text = pattern.sub('1', text)

	pattern = re.compile(r'o')
	text = pattern.sub('0', text)

	pattern = re.compile(r'l')
	text = pattern.sub('|', text)

	pattern = re.compile(r'\bs')
	text = pattern.sub('$', text)

	pattern = re.compile(r's')
	text = pattern.sub('5', text)

	pattern = re.compile(r'\.')
	text = pattern.sub('5w33t!', text)

	return text

# 25
# Pig Latin is a simple transformation of English text. Each word of the text is converted 
# as follows: move any consonant (or consonant cluster) that appears at the start of the word 
# to the end, then append ay.
# Write a function to convert a word to Pig Latin.
# Write code that converts text, instead of individual words.
# Extend it further to preserve capitalization, to keep qu together (i.e. so that quiet 
# becomes ietquay), and to detect when y is used as a consonant (e.g. yellow) vs a vowel (e.g. style).

def word_in_pig_latin(word):
	print word
	pattern = re.compile(r'^qu')
	if re.findall(pattern, word):
		orig_end = re.findall(pattern, word)
		word = pattern.sub('', word)
		word = word + orig_end[0] + 'ay'
		if orig_end[0].isupper():
			word = word[0].upper() + word[1:].lower()
		return word

	elif re.findall(r'[^aeiouAEIOU]y[^aeiouAEIOU]', word):
		pattern = re.compile(r'^[^aeiouyAEIOUY]')
		orig_end = re.findall(pattern, word)
		word = pattern.sub('', word)
		word = word + orig_end[0] + 'ay'
		if orig_end[0].isupper():
			word = word[0].upper() + word[1:].lower()
		return word

	elif re.findall(r'^[^aeiouAEIOU]+', word):
		pattern = re.compile(r'^[^aeiouAEIOU]+')
		orig_end = re.findall(pattern, word)
		print orig_end
		word = pattern.sub('', word)
		word = word + orig_end[0] + 'ay'
		if orig_end[0].isupper():
			word = word[0].upper() + word[1:].lower()
		return word

def text_in_pig_latin(text):
	tokens = text.split(' ')
	print tokens
	pig_latin_text = []
	for token in tokens:
		pig_latin_text.append(word_in_pig_latin(token))
	return pig_latin_text

# print text_in_pig_latin('This is my sentence which will be pig latin yellow style')


# 26
# Download some text from a language that has vowel harmony (e.g. Hungarian), extract the vowel 
# sequences of words, and create a vowel bigram table.

def vowel_bigram_table():
	words = udhr.words('Hungarian_Magyar-Latin1')
	all_bigrams = []
	for word in words:
		vowels = []
		for letter in word:
			if letter in "aeiou":
				vowels.append(letter)
		all_bigrams.extend(nltk.bigrams(vowels))
	cfd = nltk.ConditionalFreqDist(all_bigrams)
	cfd.tabulate()
	return

# vowel_bigram_table()

# 27
# Python's random module includes a function choice() which randomly chooses an item from a sequence, 
# e.g. choice("aehh ") will produce one of four possible characters, with the letter h being twice as 
# frequent as the others. Write a generator expression that produces a sequence of 500 randomly chosen 
# letters drawn from the string "aehh ", and put this expression inside a call to the ''.join() function, 
# to concatenate them into one long string. You should get a result that looks like uncontrolled 
# sneezing or maniacal laughter: he  haha ee  heheeh eha. Use split() and join() again to normalize 
# the whitespace in this string.

def random_sneeze():
	sneeze_list = [choice("aehh ") for i in range(500)]
	sneeze = ''.join(sneeze_list)
	sneeze = sneeze.split(' ')
	sneeze = ''.join(sneeze)
	return sneeze

# print random_sneeze()

# 28
# Consider the numeric expressions in the following sentence from the MedLine Corpus: The corresponding 
# free cortisol fractions in these sera were 4.53 +/- 0.15% and 8.16 +/- 0.23%, respectively. Should we 
# say that the numeric expression 4.53 +/- 0.15% is three words? Or should we say that it's a single 
# compound word? Or should we say that it is actually nine words, since it's read "four point five three, 
# plus or minus zero point fifteen percent"? Or should we say that it's not a "real" word at all, since 
# it wouldn't appear in any dictionary? Discuss these different possibilities. Can you think of application 
# domains that motivate at least two of these answers?

# You could argue that it should be counted all as one word because it is a single cohesive numerical
# expression.  This could be useful if you are really only interested in the language, so you could easily
# remove the expression with one go.  You could also argue that it should be three words because the 
# expression is derived from three smaller expressions.  This would be useful if you are interested
# in more math-based analytics.

# 29
# Readability measures are used to score the reading difficulty of a text, for the purposes of selecting 
# texts of appropriate difficulty for language learners. Let us define uw to be the average number of 
# letters per word, and us to be the average number of words per sentence, in a given text. The Automated 
# Readability Index (ARI) of the text is defined to be: 4.71 uw + 0.5 us - 21.43. Compute the ARI score 
# for various sections of the Brown Corpus, including section f (lore) and j (learned). Make use of the 
# fact that nltk.corpus.brown.words() produces a sequence of words, while nltk.corpus.brown.sents() 
# produces a sequence of sentences.

def calculate_ari(category):
	num_words = len(nltk.corpus.brown.words(categories=category))
	num_letters = len(''.join(nltk.corpus.brown.words(categories=category)))
	avg_letters_in_word = num_letters * 1.0 / num_words

	num_sentences = len(nltk.corpus.brown.sents(categories=category))
	avg_words_in_sent = num_words * 1.0 / num_sentences

	return (4.71 * avg_letters_in_word) + (0.5 * avg_words_in_sent) - 21.43

# print calculate_ari('lore')
# print calculate_ari('learned')

# 30
# Use the Porter Stemmer to normalize some tokenized text, calling the stemmer on each word. Do the 
# same thing with the Lancaster Stemmer and see if you observe any differences.

def use_stemmer(tokenized_text):
	porter_stemmer = nltk.PorterStemmer()
	lancaster_stemmer = nltk.LancasterStemmer()

	porter_norm = []
	lancaster_norm = []
	for word in tokenized_text:
		porter_norm.append(porter_stemmer.stem(word))
		lancaster_norm.append(lancaster_stemmer.stem(word))

	return porter_norm, lancaster_norm

# use_stemmer(...)

# 31
# Define the variable saying to contain the list ['After', 'all', 'is', 'said', 'and', 'done', ',', 'more',
# 'is', 'said', 'than', 'done', '.']. Process this list using a for loop, and store the length of each
#  word in a new list lengths. Hint: begin by assigning the empty list to lengths, using lengths = []. 
#  Then each time through the loop, use append() to add another length value to the list. Now do the same 
#  thing using a list comprehension.

def list_exercise():
	saying = ['After', 'all', 'is', 'said', 'and', 'done', ',', 'more', 'is', 'said', 'than', 'done', '.']
	lengths = []
	for word in saying:
		lengths.append(len(word))

	lengths2 = [len(word) for word in saying]
	return lengths, lengths2

# print list_exercise()

# 32
# Define a variable silly to contain the string: 'newly formed bland ideas are inexpressible in an 
# infuriating way'. (This happens to be the legitimate interpretation that bilingual English-Spanish 
# 	speakers can assign to Chomsky's famous nonsense phrase, colorless green ideas sleep furiously 
# 	according to Wikipedia). Now write code to perform the following tasks:

# Split silly into a list of strings, one per word, using Python's split() operation, and save this to 
# a variable called bland.
# Extract the second letter of each word in silly and join them into a string, to get 'eoldrnnnna'.
# Combine the words in bland back into a single string, using join(). Make sure the words in the 
# resulting string are separated with whitespace.
# Print the words of silly in alphabetical order, one per line.

def list_exercise2():
	silly = 'newly formed bland ideas are inexpressible in an infuriating way'
	bland = silly.split(' ')
	second_letters = ''.join([word[1] for word in bland])
	print second_letters

	for word in sorted(bland):
		print word
	return 

# list_exercise2()

# 33
# The index() function can be used to look up items in sequences. For example, 'inexpressible'.index('e') 
# tells us the index of the first position of the letter e.

# What happens when you look up a substring, e.g. 'inexpressible'.index('re')?
# Define a variable words containing a list of words. Now use words.index() to look up the position of 
# an individual word.
# Define a variable silly as in the exercise above. Use the index() function in combination with list 
# slicing to build a list phrase consisting of all the words up to (but not including) in in silly.

def list_exercise3():
	print 'inexpressible'.index('re')
	words = ['newly', 'formed', 'bland', 'ideas', 'are', 'inexpressible', 'in', 'an', 'infuriating', 'way']
	print words.index('are')
	in_index = words.index('in')
	sliced_list = words[:in_index]
	print sliced_list
	return
# list_exercise3()

# 34
# Write code to convert nationality adjectives like Canadian and Australian to their corresponding 
# nouns Canada and Australia 

# The link they give definitely does not give enough information to complete this, but my approach would
# be to somehow get a list of country names and a list of regex that fit the patterns normally found
# at the end of nationality adjective.  I would replace those patterns with the suggested ending that
# they came from, then check to make sure the new word was in the list of country names.

# 35
# Investigate this phenomenon with the help of a corpus and the findall() method for searching 
# tokenized text described in 3.5
def phrase_search():
	text = nltk.Text(nltk.corpus.brown.words(categories=['romance', 'religion']))
	print text.findall(r"<as> <best> <as> <he|she|it|I|you> <can> <\w*s>")
	return

# phrase_search()

# 36
# Study the lolcat version of the book of Genesis, accessible as nltk.corpus.genesis.words('lolcat.txt'), and the rules
#  for converting text into lolspeak at http://www.lolcatbible.com/index.php?title=How_to_speak_lolcat.
#  Define regular expressions to convert English words into corresponding lolspeak words.

# 'i' -> 'ai', 'dude' -> 'dood', 'er ' -> 'r ', 'y ' -> 'eh ', 'th' -> 'f'
# use re.compile(r'(...)')

# 37
# Read about the re.sub() function for string substitution using regular expressions, using help(re.sub) 
# and by consulting the further readings for this chapter. Use re.sub in writing code to remove HTML tags 
# from an HTML file, and to normalize whitespace.

def remove_html_tags(text):
	regex = re.compile(r'<[^>]+>')
	new_text = regex.sub('', text)
	regex2 = re.compile(r'\s')
	new_text2 = regex2.sub(' ', new_text)
	return new_text2

# print remove_html_tags('hello this is a <test>')

# 38
# An interesting challenge for tokenization is words that have been split across a line-break. E.g. 
# if long-term is split, then we have the string long-\nterm.

# Write a regular expression that identifies words that are hyphenated at a line-break. The expression 
# will need to include the \n character.
# Use re.sub() to remove the \n character from these words.
# How might you identify words that should not remain hyphenated once the newline is removed,
# e.g. 'encyclo-\npedia'?

def line_break(text):
	regex = re.compile(r'[a-zA-Z]+/n[a-zA-Z]+')
	print regex.sub('', text)

# use a dictionary to check if the hyphen is in the actual word.

# 39
# Read the Wikipedia entry on Soundex. Implement this algorithm in Python.
def soundex(word):
	first_let = word[0]
	rem_hw = str([word[i] for i in range(1, len(word)) if ((word[i] == 'h') or (word[i] == 'w'))])
	rem_hw = first_let + rem_hw

	consonant_changes = [['[bfpv]', '1'], ['[cgjkqsxz]', '2'], ['[dt]', '3'], ['l', '4'], ['[mn]', '5'], ['r', '6']]

	const_replaced = ""
	for change in consonant_changes:
		# not working??
		pattern = re.compile(r'(' + consonant_changes[0] +')')
		const_replaced = pattern.sub(consonant_changes[1], word)

	i = 0
	len_word = len(const_replaced)
	while (i < len_word - 1):
		if (const_replaced[i] == const_replaced[i+1]):
			const_replaced = const_replaced[:i] + const_replaced[i+1:]
			len_word = len_word - 1

	new_word = const_replaced[1:]
	# remove vowels
	vowels = re.compile(r'[aeiou]')
	new_word = vowels.sub('', new_word)
	# add back first letter
	new_word = first_let + new_word

	return new_word

# 40
# Obtain raw texts from two or more genres and compute their respective reading difficulty scores as 
# in the earlier exercise on reading difficulty. E.g. compare ABC Rural News and ABC Science News 
# (nltk.corpus.abc).

def diff_scores():
	num_words = len(nltk.corpus.abc.words())
	num_letters = len(''.join(nltk.corpus.abc.words()))
	avg_letters_in_word = num_letters * 1.0 / num_words

	num_sentences = len(nltk.corpus.abc.sents())
	avg_words_in_sent = num_words * 1.0 / num_sentences

	return (4.71 * avg_letters_in_word) + (0.5 * avg_words_in_sent) - 21.43

# print diff_scores()

# 41
 # Rewrite the following nested loop as a nested list comprehension:

def nested_list_comp():
	words = ['attribution', 'confabulation', 'elocution', 'sequoia', 'tenacious', 'unidirectional']
	vowels = [''.join([letter for letter in word if (letter in 'aeiou')]) for word in words]
	return vowels

# print nested_list_comp()

# 42
# Use WordNet to create a semantic index for a text collection. Extend the concordance search program 
# in 3.6, indexing each word using the offset of its first synset, e.g. wn.synsets('dog')[0].offset 
# (and optionally the offset of some of its ancestors in the hypernym hierarchy).

class IndexedText(object):

    def __init__(self, stemmer, text):
        self._text = text
        self._stemmer = stemmer
        # I am indexing according to the offsets.  Not sure what could be wrong here,
        # but I am getting IndexError: list index out of range.  Stack Overflow suggested this
        # could be due to whitespace issues, but stripping the text of whitespace doesn't help.
        self._index = nltk.Index((self._stem(word), wn.synsets(word)[0].offset()) for word in text)

    def concordance(self, word, width=40):
        key = self._stem(word)
        wc = int(width/4)                # words of context
        for i in self._index[key]:
            lcontext = ' '.join(self._text[i-wc:i])
            rcontext = ' '.join(self._text[i:i+wc])
            ldisplay = '{:>{width}}'.format(lcontext[-width:], width=width)
            rdisplay = '{:{width}}'.format(rcontext[:width], width=width)
            print(ldisplay, rdisplay)

    def _stem(self, word):
        return self._stemmer.stem(word).lower()

def semantic_index():
	porter = nltk.PorterStemmer()
	grail = nltk.corpus.webtext.words('grail.txt')
	text = IndexedText(porter, grail)
	return text.concordance('lie')

# print semantic_index()

# 43
# With the help of a multilingual corpus such as the Universal Declaration of Human Rights Corpus 
# (nltk.corpus.udhr), and NLTK's frequency distribution and rank correlation functionality (nltk.FreqDist, 
#  nltk.spearman_correlation), develop a system that guesses the language of a previously unseen text. 
# For simplicity, work with a single character encoding and just a few languages.

def guess_language(text):
	languages = ['English-Latin1', 'Spanish-Latin1']
	language_tally = defaultdict(int)
	for word in text:
		for language in languages:
			if word in nltk.corpus.udhr.words(language):
				language_tally[language] += 1

	return max(language_tally, key=language_tally.get)

# print guess_language(['hello', 'world'])
# print guess_language(['hola', 'mundo'])

# 44
# Write a program that processes a text and discovers cases where a word has been used with a novel sense. 
# For each word, compute the WordNet similarity between all synsets of the word and all synsets of the 
# words in its context. (Note that this is a crude approach; doing it well is a difficult, open research 
# problem.)

def novel_sense(text):
	similarities = defaultdict(int)
	for word in text:
		synsets = wn.synsets(word)
		for synset in synsets:
			for context_word in text[text.index(word)-2:text.index(word)+3]
				for context_synset in wn.synsets(context_word)
					similarities[word] += synset.res_similarity(context_synset, text)
	return similarities

# 45
# Broken link.
