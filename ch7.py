# NLP Chapter 7 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
from nltk.chunk import *
from nltk.chunk.util import *

# 1
# The IOB format categorizes tagged tokens as I, O and B. Why are three tags necessary? What problem would be caused if we used I 
# and O tags exclusively?

# We need the three tags to be able to mark the beginning of each chunk.  For example, if we have two chunks of the same type,
# we need to somehow indicate that they are separate chunks.  We can only do this if we have a marker for the beginning of the 
# chunks; otherwise, there will be no way to distinguish the boundary between them.

# 2
# Write a tag pattern to match noun phrases containing plural head nouns, e.g. "many/JJ researchers/NNS", "two/CD weeks/NNS", 
# "both/DT new/JJ positions/NNS". Try to do this by generalizing the tag pattern that handled singular noun phrases.

# "NP: {<DT>?<JJ>*<NNS>}"

# 3
# Pick one of the three chunk types in the CoNLL corpus. Inspect the CoNLL corpus and try to observe any patterns in the POS tag 
# sequences that make up this kind of chunk. Develop a simple chunker using the regular expression chunker nltk.RegexpParser. 
# Discuss any tag sequences that are difficult to chunk reliably.

def prob_3():
	grammar = r"VP: {<[V].*>+}"
	cp = nltk.RegexpParser(grammar)
	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['VP'])
	print(cp.evaluate(test_sents))

	# ChunkParse score:
 #    IOB Accuracy:  92.8%
 #    Precision:     59.6%
 #    Recall:        68.9%
 #    F-Measure:     63.9%

# prob_3()
# It is difficult to chunk any tag sequence that doesn't begin with a verb because these are hard to classify.  The most simple
# and common version of a verb phrase starts with a verb itself, so I chose to focus on this one in order to capture the greatest
# possible number of chunks in one simple grammar.

# 4
# An early definition of chunk was the material that occurs between chinks. Develop a chunker that starts by putting the whole 
# sentence in a single chunk, and then does the rest of its work solely by chinking. Determine which tags (or tag sequences) are 
# most likely to make up chinks with the help of your own utility program. Compare the performance and simplicity of this approach 
# relative to a chunker based entirely on chunk rules.

def prob_4():
	grammar = r"""NP:{<.*>+}
	}<VBD|IN>+{
	"""
	cp = nltk.RegexpParser(grammar)
	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['NP'])
	print(cp.evaluate(test_sents))

	# ChunkParse score:
 #    IOB Accuracy:  58.1%
 #    Precision:     26.0%
 #    Recall:        17.2%
 #    F-Measure:     20.7%

 # The chunking approach is simpler because it is actually searching for matches to find what we want rather than searching for
 # things that don't match.  Also, this version takes longer to run than the chunker version due to the extra step of
 # putting everything in one chunk before doing any real work.

# prob_4()

# 5
# Write a tag pattern to cover noun phrases that contain gerunds, e.g. "the/DT receiving/VBG end/NN", "assistant/NN managing/VBG 
# editor/NN". Add these patterns to the grammar, one per line. Test your work using some tagged sentences of your own devising.

def prob_5():
	sent1 = [("I", "NN"), ("am", "VBS"), ("on", "PP"), ("the", "DT"), ("receiving", "VBG"), ("end", "NN")]
	sent2 = [("Get", "VBD"), ("me", "NN"), ("the", "DET"), ("assistant", "NN"), ("managing", "VBG"), ("editor", "NN")]
	grammar = r"NPG: {<DT>?<NN>?<VBG><NN>}"
	cp = nltk.RegexpParser(grammar)
	# cp.parse(sent1).draw()
	cp.parse(sent2).draw()

# prob_5()

# 6
# Write one or more tag patterns to handle coordinated noun phrases, e.g. "July/NNP and/CC August/NNP", "all/DT your/PRP$ managers/NNS 
# and/CC supervisors/NNS", "company/NN courts/NNS and/CC adjudicators/NNS".

def prob_6():
	sent1 = [("It", "PP"), ("happens", "VBS"), ("in", "IN"), ("July", "NNP"), ("and", "CC"), ("August", "NNP")]
	sent2 = [("I", "NN"), ("talked", "VBD"), ("to", "TO"), ("all", "DT"), ("your", "PRP$"), ("managers", "NNS"), ("and", "CC"), ("supervisors", "NNS")]
	sent3 = [("I", "NN"), ("work", "VBD"), ("with", "PP"), ("company", "NN"), ("courts", "NNS"), ("and", "CC"), ("adjudicators", "NNS")]
	grammar = r"Coord NP: {<NNP><CC><NNP>|<DT><PRP\$><NNS><CC><NNS>|<NN><NNS><CC><NNS>}"
	cp = nltk.RegexpParser(grammar)
	# cp.parse(sent1).draw()
	# cp.parse(sent2).draw()
	cp.parse(sent3).draw()

# prob_6()

# 7
# Carry out the following evaluation tasks for any of the chunkers you have developed earlier. (Note that most chunking 
# corpora contain some internal inconsistencies, such that any reasonable rule-based approach will produce errors.)
# Evaluate your chunker on 100 sentences from a chunked corpus, and report the precision, recall and F-measure.
# Use the chunkscore.missed() and chunkscore.incorrect() methods to identify the errors made by your chunker. Discuss.
# Compare the performance of your chunker to the baseline chunker discussed in the evaluation section of this chapter.

def prob_7():
	grammar = r"VP: {<[V].*>+}"
	cp = nltk.RegexpParser(grammar)
	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['VP'])[:100]
	print(cp.evaluate(test_sents))

	# can't get the chunkscore.missed or chunkscore.incorrect working
	# Things that could be errors: gerunds!  They have a verb tag in them but are not parts of verb phrases.

	# ChunkParse score:
 #    IOB Accuracy:  95.0%
 #    Precision:     64.7%
 #    Recall:        77.7%
 #    F-Measure:     70.6%

 # Comparison to baseline: 
 # ChunkParse score:
 #    IOB Accuracy:  43.4%
 #    Precision:      0.0%
 #    Recall:         0.0%
 #    F-Measure:      0.0%

 # In comparison, my chunker does a much better job than the baseline chunker in all aspects.

# prob_7()

# 8
# Develop a chunker for one of the chunk types in the CoNLL corpus using a regular-expression based chunk grammar 
# RegexpChunk. Use any combination of rules for chunking, chinking, merging or splitting.

def prob_8():
	chunk_rule = ChunkRule("<.*>+", "Chunk everything")
	chink_rule = ChinkRule("<VBD|IN|\.>", "Chink on verbs/prepositions")
	split_rule = SplitRule("<DT><NN>", "<DT><NN>", "Split successive determiner/noun pairs")

	chunk_parser = nltk.RegexpChunkParser([chunk_rule], chunk_label='NP')

# 9
# Sometimes a word is incorrectly tagged, e.g. the head noun in "12/CD or/CC so/RB cases/VBZ". Instead of requiring 
# manual correction of tagger output, good chunkers are able to work with the erroneous output of taggers. Look for 
# other examples of correctly chunked noun phrases with incorrect tags.

# To do this, you have to go through all the data (after being chunked) and check all the tags as well
# as the output of the chunker.
# I scrolled through some of them and couldn't find any instances of this.  This could be attributed to the fact that
# the chunker I looked at wasn't a "good chunker", in that it couldn't deal with the erroneous tagger output.

# 10
# The bigram chunker scores about 90% accuracy. Study its errors and try to work out why it doesn't get 100% accuracy. 
# Experiment with trigram chunking. Are you able to improve the performance any more?

def tag_chunks(chunked_sents):
    tagged_sents = [nltk.chunk.tree2conlltags(tree) for tree in chunked_sents]
    return [[(t, c) for (w, t, c) in chunk_tags] for chunk_tags in tagged_sents]

def prob_10():
	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['NP'])
	train_sents = nltk.corpus.conll2000.chunked_sents('train.txt', chunk_types=['NP'])
	train_tags = tag_chunks(train_sents)
	test_tags = tag_chunks(test_sents)
	unigram_chunker = nltk.tag.UnigramTagger(train_tags)
	bigram_chunker = nltk.tag.BigramTagger(train_tags, backoff=unigram_chunker)
	trigram_chunker = nltk.tag.TrigramTagger(train_tags, backoff=bigram_chunker)
	print(trigram_chunker.evaluate(test_tags))
	# one issue: the bigram chunker doesn't have a backoff, so we can add this
	# also, a trigram tagger should be more accurate just because it looks at a wider area of tags

	# new accuracy: 0.934672942567

# prob_10()

# 11
# Apply the n-gram and Brill tagging methods to IOB chunk tagging. Instead of assigning POS tags to words, here we will 
# assign IOB tags to the POS tags. E.g. if the tag DT (determiner) often occurs at the start of a chunk, it will be 
# tagged B (begin). Evaluate the performance of these chunking methods relative to the regular expression chunking 
# methods covered in this chapter.

# ngram taggers are given in the previous question

def train_brill_tagger(initial_tagger, train_sents, **kwargs):
    templates = [nltk.tag.brill.Template(nltk.tag.brill.Pos([-1]))]
    trainer = nltk.tag.brill_trainer.BrillTaggerTrainer(initial_tagger, templates, trace=3,)
    return trainer.train(train_sents, **kwargs)

# generating ((word, pos),iob) pairs as feature.
def chunk_trees2train_chunks(chunk_sents):
    tag_sents = [tree2conlltags(sent) for sent in chunk_sents]
    return [[((w,t),c) for (w,t,c) in sent] for sent in tag_sents]

def prob_11():
	tagger = nltk.tag.DefaultTagger('NN')
	train_sents = nltk.corpus.treebank_chunk.chunked_sents()[:100]
	test_sents = nltk.corpus.treebank_chunk.chunked_sents()[100:120]

	t = chunk_trees2train_chunks(train_sents)
	bt = train_brill_tagger(tagger, t)
	print(bt.evaluate(chunk_trees2train_chunks(test_sents)))

	# accuracy: 0.424778761062 (very bad compared to the previous chunking methods, like regex)

# prob_11()

# 12
# We saw in 5. that it is possible to establish an upper limit to tagging performance by looking for ambiguous n-grams, 
# n-grams that are tagged in more than one possible way in the training data. Apply the same method to determine an upper 
# bound on the performance of an n-gram chunker.

def prob_12():
	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['NP'])
	train_sents = nltk.corpus.conll2000.chunked_sents('train.txt', chunk_types=['NP'])[:100]
	train_tags = tag_chunks(train_sents)
	test_tags = tag_chunks(test_sents)

	cfd = nltk.ConditionalFreqDist(((x[1], y[1], z[0]), z[1]) 
		for sent in train_tags 
		for x, y, z in nltk.trigrams(sent))

	for c in cfd.conditions():
		print c

	ambiguous_contexts = [c for c in cfd.conditions() if len(cfd[c]) > 1]
	print(sum(cfd[c].N() for c in ambiguous_contexts) / cfd.N())

# prob_12()

# 13
# Pick one of the three chunk types in the CoNLL corpus. Write functions to do the following tasks for your chosen type:
# List all the tag sequences that occur with each instance of this chunk type.
# Count the frequency of each tag sequence, and produce a ranked list in order of decreasing frequency; each line should 
# consist of an integer (the frequency) and the tag sequence.
# Inspect the high-frequency tag sequences. Use these as the basis for developing a better chunker.

def prob_13():
	train_sents = nltk.corpus.conll2000.chunked_sents('train.txt', chunk_types=['NP'])[:100]
	tuples = list()
	for sent in train_sents:
		triplet = ("<START>", "<START>", "<START>")
		for entry in sent:
			print entry
			try:
				triplet = (triplet[1], triplet[2], entry)
				if entry.node == 'NP':
					tuples.append(triplet)
			except:
				triplet = (triplet[1], triplet[2], entry)

	fdist = nltk.probability.FreqDist(tuples)
	for seq in fdist:
		print fdist[seq], seq

# prob_13()

# 14
# The baseline chunker presented in the evaluation section tends to create larger chunks than it should. For example, the 
# phrase: [every/DT time/NN] [she/PRP] sees/VBZ [a/DT newspaper/NN] contains two consecutive chunks, and our baseline 
# chunker will incorrectly combine the first two: [every/DT time/NN she/PRP]. Write a program that finds which of these 
# chunk-internal tags typically occur at the start of a chunk, then devise one or more rules that will split up these 
# chunks. Combine these with the existing baseline chunker and re-evaluate it, to see if you have discovered an improved 
# baseline.

def prob_14():
	sent1 = [("every", "DT"), ("time", "NN"), ("she", "PRP"), ("sees", "VBZ"), ("a", "DT"), ("newspaper", "NN")]
	grammar = r"NP: {<DT><NN>|<PRP>}"
	cp = nltk.RegexpParser(grammar)
	# this shows that it works on this example
	# comment out this draw line to see the accuracy stats
	# cp.parse(sent1).draw()

	test_sents = nltk.corpus.conll2000.chunked_sents('test.txt', chunk_types=['NP'])[:100]
	print(cp.evaluate(test_sents))

	# ChunkParse score:
 #    IOB Accuracy:  47.0%
 #    Precision:     83.1%
 #    Recall:        16.7%
 #    F-Measure:     27.8%

 # Clearly this does not improve the baseline tagger, probably because it overfits to the provided example issue.

# prob_14()

# 15
# Develop an NP chunker that converts POS-tagged text into a list of tuples, where each tuple consists of a verb 
# followed by a sequence of noun phrases and prepositions, e.g. the little cat sat on the mat becomes 
# ('sat', 'on', 'NP')...

def prob_15():
	grammar = r"""NPHRASE:{<DT>?<JJ.*>*<NN.*>+}
	V: {<VB><IN><NPHRASE>} 
	"""
	cp = nltk.RegexpParser(grammar)
	tuples = set()
	for sent in nltk.corpus.brown.tagged_sents():
		tree = cp.parse(sent)
		for other_tree in tree.subtrees():
			if other_tree.label == "V":
				tuples.add((other_tree[0][0], other_tree[1][0], "NP"))

# prob_15()


# 16
# The Penn Treebank contains a section of tagged Wall Street Journal text that has been chunked into noun phrases. 
# The format uses square brackets, and we have encountered it several times during this chapter. The Treebank corpus 
# can be accessed using: for sent in nltk.corpus.treebank_chunk.chunked_sents(fileid). These are flat trees, just as 
# we got using nltk.corpus.conll2000.chunked_sents().
# The functions nltk.tree.pprint() and nltk.chunk.tree2conllstr() can be used to create Treebank and IOB strings from 
# a tree. Write functions chunk2brackets() and chunk2iob() that take a single chunk tree as their sole argument, and 
# return the required multi-line string representation.

def chunk2brackets(chunk_tree):
	# ISSUE: THIS PPRINT FUNCTION DOES NOT EXIST IN THIS MODULE.
	return nltk.tree.pprint(chunk_tree)
def chunk2iob(chunk_tree):
	return nltk.chunk.tree2conllstr(chunk_tree);

def prob_16():
	train_sents = nltk.corpus.conll2000.chunked_sents('train.txt', chunk_types=['NP'])[:2]
	for sent in train_sents:
		# print chunk2brackets(sent)
		print chunk2iob(sent)

# prob_16()

# 17
# An n-gram chunker can use information other than the current part-of-speech tag and the n-1 previous chunk tags. 
# Investigate other models of the context, such as the n-1 previous part-of-speech tags, or some combination of 
# previous chunk tags along with previous and following part-of-speech tags.

# I think the most useful model of the context would include as much information as possible.
# For example, the chunker should know about this history of the part of speech tags and the history of the 
# chunk tags.  However, we should be careful about how much context we provide to the chunker because we do not
# want to overfit it to the training data.  If this were to happen, we chunker would learn a lot of rules
# that were too specific to the training data, and it would perform poorly on its chunking of the testing data.

# 18
# Consider the way an n-gram tagger uses recent tags to inform its tagging choice. Now observe how a chunker may re-use 
# this sequence information. For example, both tasks will make use of the information that nouns tend to follow 
# adjectives (in English). It would appear that the same information is being maintained in two places. Is this 
# likely to become a problem as the size of the rule sets grows? If so, speculate about any ways that this problem 
# might be addressed.

# Yes, this could be a problem as the size of the rule sets grows because the chunker will take longer and longer
# to find rules that match the data we are trying to chunk.  One way to address this issue would be to make sure that
# the chunker only stores rules that have not already been defined by the tagger.  This way, the rules are not
# duplicated, so we only search through the rules once instead of twice everytime.

