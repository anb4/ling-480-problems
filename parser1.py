% start S
############################
# Grammar Rules
#############################

S[SEM = <?subj(?vp)>] -> NP[NUM=?n,SEM=?subj] VP[NUM=?n,SEM=?vp]

NP[NUM=?n,SEM=<?det(?nom)> ] -> Det[SEM=?det]  Nom[NUM=?n,SEM=?nom]
NP[LOC=?l,NUM=?n,SEM=?np] -> PropN[LOC=?l,NUM=?n,SEM=?np]

Nom[NUM=?n,SEM=?nom] -> N[SEM=?nom]

VP[NUM=?n,SEM=<?v(?obj)>] -> TV[NUM=?n,SEM=?v] NP[SEM=?obj]
VP[NUM=?n,SEM=<?v(?obj,?pp)>] -> IV[NUM=?n,SEM=?v] PP[+WITH,SEM=?pp]

PP[+TO, SEM=?np] -> P[+TO] NP[SEM=?np]

#############################
# Lexical Rules
#############################

PropN[-LOC,NUM=sg,SEM=<\P.P(angus)>] -> 'Judy'
PropN[-LOC,NUM=sg,SEM=<\P.P(cyril)>] -> 'Paul'

Det[SEM=<\P Q.exists x.(P(x) & Q(x))>] -> 'the'

N[SEM=<\x.bunny(x)>] -> 'bunny'
N[SEM=<\x.she(x)>] -> 'she'

TV[NUM=sg,SEM=<\X x.X(\y.pat(x,y))>,TNS=pres] -> 'pats'
IV[NUM=sg,SEM=<\X x.X(\y.play(x,y))>,TNS=pres] -> 'plays'

PP[+WITH, SEM=?np] -> P[+WITH] NP[SEM=?np]
P[+WITH] -> 'with'