# NLP Chapter 5 Solutions
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
import pylab
from collections import defaultdict


# 1
# Search the web for "spoof newspaper headlines", to find such gems as: British Left Waffles on Falkland Islands, and 
# Juvenile Court to Try Shooting Defendant. Manually tag these headlines to see if knowledge of the part-of-speech tags 
# removes the ambiguity.

# sense 1: tagging resolves ambiguity
sense1a = [('British', 'ADJ'), ('Left', 'N'), ('Waffles', 'V'), ('on', 'ON'), ('Falkland', 'ADJ'), ('Islands', 'N')]
sense1b = [('British', 'N'), ('Left', 'V'), ('Waffles', 'N'), ('on', 'ON'), ('Falkland', 'ADJ'), ('Islands', 'N')]

# sense2: tagging does not resolve ambiguity
sense2a = [('Juvenile', 'ADJ'), ('Court', 'N'), ('to', 'TO'), ('Try', 'V'), ('Shooting', 'ADJ'), ('Defendant', 'N')]
sense2b = [('Juvenile', 'ADJ'), ('Court', 'N'), ('to', 'TO'), ('Try', 'V'), ('Shooting', 'ADJ'), ('Defendant', 'N')]

# 2
# Working with someone else, take turns to pick a word that can be either a noun or a verb (e.g. contest); the opponent has 
# to predict which one is likely to be the most frequent in the Brown corpus; check the opponent's prediction, and tally the score 
# over several turns.

def check_freq_pos(check_word):
	brown_tagged = nltk.corpus.brown.tagged_words(tagset='universal')
	tag_fd = nltk.FreqDist(tag for (word, tag) in brown_tagged if word == check_word)
	print tag_fd.most_common()

# check_freq_pos("contest")

# 3
# Tokenize and tag the following sentence: They wind back the clock, while we chase after the wind. What different pronunciations 
# and parts of speech are involved?

tag_sent = [('They', 'PRON'), ('wind', 'V'), ('back', 'ADV'), ('the', 'DET'), ('clock', 'N'), (',', ','), ('while', 'ADV'), ('we', 'PRON'), ('chase', 'V'), ('after', 'ADP'), ('the', 'DET'), ('wind', 'N')]

# 4
# Review the mappings in 3.1. Discuss any other examples of mappings you can think of. What type of information do they map 
# from and to?
# Translation mappings map from a word sense in one language to the other.  Antonym mappings map from a word sense to its antonyms.

# 5
# Using the Python interpreter in interactive mode, experiment with the dictionary examples in this chapter. Create a dictionary d, 
# and add some entries. What happens if you try to access a non-existent entry, e.g.  d['xyz']?

# d = {}
# print d['xyz'] gives a KeyError

# 6
# Try deleting an element from a dictionary d, using the syntax del d['abc']. Check that the item was deleted.
def prob_6():
	d = {}
	d['abc'] = 1
	print d
	del d['abc']
	print d
# prob_6()

# 7
# Create two dictionaries, d1 and d2, and add some entries to each. Now issue the command d1.update(d2). What did this do? 
# What might it be useful for?

def prob_7():
	d1 = {'abc': 1, 'def': 2}
	d2 = {'abc': 3}
	d1.update(d2)
	print d1
	print d2
# Updates the values of d1 with the values of d2.  Useful for information needed to be collected separately but stored together.
# prob_7()

# 8
# Create a dictionary e, to represent a single lexical entry for some word of your choice. Define keys like headword, part-of-speech, 
# sense, and example, and assign them suitable values.

def prob_8():
	e = {'headword': 0, 'part-of-speech': 1, 'sense': 2, 'example': 3}

# 9
# Satisfy yourself that there are restrictions on the distribution of go and went, in the sense that they cannot be 
# freely interchanged in the kinds of contexts illustrated in (3d) in 7.
# Yesterday, we ____ on the excursion.  --> only 'went' is allowed here, not 'go'

# 10
# Train a unigram tagger and run it on some new text. Observe that some words are not assigned a tag. Why not?
# Some words are not assigned a tag because they have not been previously seen and there is no backup.

def prob_10():
	brown_tagged_sents = nltk.corpus.brown.tagged_sents(categories='news')
	brown_sents = nltk.corpus.brown.sents(categories='news')
	unigram_tagger = nltk.UnigramTagger(brown_tagged_sents)
	print unigram_tagger.tag(nltk.corpus.brown.sents(categories='editorial')[201])

# prob_10()

# 11
# Learn about the affix tagger (type help(nltk.AffixTagger)). Train an affix tagger and run it on some new text. Experiment with 
# different settings for the affix length and the minimum word length. Discuss your findings.

def prob_11():	
	brown_tagged_sents = nltk.corpus.brown.tagged_sents(categories='news')
	affix_tagger = nltk.AffixTagger(brown_tagged_sents, affix_length = -2, min_stem_length = 5)
	print affix_tagger.tag(nltk.corpus.brown.sents(categories='editorial')[201])

# this isn't able to tag many words, and the words it tags look to be incorrect.  this is regardless of the parameters.
# prob_11()

# 12
# Train a bigram tagger with no backoff tagger, and run it on some of the training data. Next, run it on some new data. 
# What happens to the performance of the tagger? Why?

def prob_12():
	brown_tagged_sents = nltk.corpus.brown.tagged_sents(categories='news')
	bigram_tagger = nltk.BigramTagger(brown_tagged_sents)
	print bigram_tagger.tag(nltk.corpus.brown.sents(categories='editorial')[201])
	print bigram_tagger.tag(brown_tagged_sents[1001])

# the new data hits Nones once one bigram cannot be tagged.  The same happens to the old data, but not as quickly.
# prob_12()

# 13
# We can use a dictionary to specify the values to be substituted into a formatting string. Read Python's library documentation 
# for formatting strings http://docs.python.org/lib/typesseq-strings.html and use this method to display today's date in two 
# different formats.

# link gives 404.
def prob_13():
	date = "1-1-1"
	date2 = 1.1
	a = "today's date: %s" % date
	print a
	b = "today's date: %d" % date2
	print b
# prob_13()

# 14
# Use sorted() and set() to get a sorted list of tags used in the Brown corpus, removing duplicates.
def prob_14():
	brown_tagged_words = nltk.corpus.brown.tagged_words()
	tags = set([tag for (word, tag) in brown_tagged_words])
	print sorted(tags)
# prob_14()

# 15
# Write programs to process the Brown Corpus and find answers to the following questions:
# Which nouns are more common in their plural form, rather than their singular form? (Only consider regular plurals, formed with the -s suffix.)
# Which word has the greatest number of distinct tags. What are they, and what do they represent?
# List tags in order of decreasing frequency. What do the 20 most frequent tags represent?
# Which tags are nouns most commonly found after? What do these tags represent?
def prob_15():
	cfd = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words(categories='editorial'))
	tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')
	just_tags = [value for key, value in tagged_words]

	# 20 most frequent tags
	fd = nltk.FreqDist(just_tags)
	common_tags = fd.most_common(20)
	print common_tags

	# nouns more common in their plural form
	conditions = cfd.conditions()
	for condition in conditions:
		if (cfd[condition]['NNS'] > cfd[condition]['NN']):
			print condition

	# tags that nouns are most likely found after
	bigrams = nltk.bigrams(tagged_words)
	nouns_after = [tag1[1] for tag1, tag2 in bigrams if tag2[1] == 'NN']

	fd2 = nltk.FreqDist(nouns_after)
	print fd2.most_common(20)

	# which word has the greatest number of distinct tags
	num_tags = [(condition, len(cfd[condition])) for condition in conditions]
	num_tags = list(reversed(sorted(num_tags, key=lambda x: x[1])))
	print num_tags[0]

# prob_15()

# 16

# Explore the following issues that arise in connection with the lookup tagger:
# What happens to the tagger performance for the various model sizes when a backoff tagger is omitted?
# Consider the curve in 4.2; suggest a good size for a lookup tagger that balances memory and performance. Can you come up 
# with scenarios where it would be preferable to minimize memory usage, or to maximize performance with no regard for memory usage?

# The tagger performance for large model sizes would do better without a backoff tagger than small models.  This is because the
# larger model will have less instances of never seeing a word before.  A model of size 12000 seems to be a good balance of
# memory and performance.  If the goal is fast tagging, maximizing performance with less regard for memory usage seems important.

# 17
# What is the upper limit of performance for a lookup tagger, assuming no limit to the size of its table? (Hint: write a program 
# 	to work out what percentage of tokens of a word are assigned the most likely tag for that word, on average.)

def prob_17():
	cfd = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words(categories='editorial'))
	tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')

	percent_most_likely = []
	for (word, freq) in cfd.items():
		num = float(freq.N())
    	max_tag = freq.max()
    	average = freq[max_tag] / num
    	percent_most_likely.append(average)
	avg_correct = 1.0 * sum(percent_most_likely) / len(percent_most_likely)
	print avg_correct

# prob_17()

# 18
# Generate some statistics for tagged data to answer the following questions:
# What proportion of word types are always assigned the same part-of-speech tag?
# How many words are ambiguous, in the sense that they appear with at least two tags?
# What percentage of word tokens in the Brown Corpus involve these ambiguous words?

def prob_18():
	cfd = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words(categories='editorial'))
	tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')
	conditions = cfd.conditions()
	ambiguous_words = []
	unambiguous_words = []
	for condition in conditions:
		if len(cfd[condition]) > 1:
			ambiguous_words.append(condition)
		else:
			unambiguous_words.append(condition)

	num_ambiguous = len(ambiguous_words)
	num_unambiguous = len(unambiguous_words)

	ambiguous_words = set(ambiguous_words)
	unambiguous_words = set(unambiguous_words)

	# proportion of word types that are always assigned the same part of speech tag
	print (float(num_unambiguous) / len(tagged_words))

	# how many words are ambiguous
	print len(ambiguous_words)

	# what percentage of word tokens involve these ambiguous words
	print (float(num_ambiguous) / len(tagged_words))

# prob_18()

# 19
# The evaluate() method works out how accurately the tagger performs on this text. For example, if the supplied tagged text was 
# [('the', 'DT'), ('dog', 'NN')] and the tagger produced the output [('the', 'NN'), ('dog', 'NN')], then the score would be 0.5. 
# Let's try to figure out how the evaluation method works:
# A tagger t takes a list of words as input, and produces a list of tagged words as output. However, t.evaluate() is given correctly 
# tagged text as its only parameter. What must it do with this input before performing the tagging?
# Once the tagger has created newly tagged text, how might the evaluate() method go about comparing it with the original tagged 
# text and computing the accuracy score?
# Now examine the source code to see how the method is implemented. Inspect nltk.tag.api.__file__ to discover the location of the 
# source code, and open this file using an editor (be sure to use the api.py file and not the compiled api.pyc binary file).

# The tagger needs to be trained first.  The evaluate() method could go through the tags one by one and compare the tagger's
# guess to the original tagged text.

# 20
# Write code to search the Brown Corpus for particular words and phrases according to tags, to answer the following questions:
# Produce an alphabetically sorted list of the distinct words tagged as MD.
# Identify words that can be plural nouns or third person singular verbs (e.g. deals, flies).
# Identify three-word prepositional phrases of the form IN + DET + NN (eg. in the lab).
# What is the ratio of masculine to feminine pronouns?

def prob_20():
	tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')
	md_tags = [word for word, tag in tagged_words if tag == "MD"]
	# alphabetical list of words tagged as MD
	print sorted(set(md_tags))

	cfd = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words(categories='editorial'))
	conditions = cfd.conditions()

	special_words = []
	for condition in conditions:
		if (cfd[condition]['NNS'] and cfd[condition]['VBZ']):
			special_words.append(condition)

	# words that can be plural nouns or 3rd person singular verbs
	print set(special_words)

	trigrams = nltk.trigrams(tagged_words)

	prep_phrases = []
	for trigram in trigrams:
		zipped = [t for t in zip(*trigram)]
		if zipped[1] == ('IN', 'DT', 'NN'):
			prep_phrases.append(zipped[0])

	# three word prepositional phrases
	print set(prep_phrases)

# prob_20()

# 21
# In 3.1 we saw a table involving frequency counts for the verbs adore, love, like, prefer and preceding qualifiers absolutely and 
# definitely. Investigate the full range of adverbs that appear before these four verbs.

def prob_21():
	tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')
	bigrams = nltk.bigrams(tagged_words)
	adverbs = [tag1[0] for tag1, tag2 in bigrams if (tag2[0] == 'adore' or tag2[0] == 'love' or tag2[0] == 'like' or tag2[0] == 'prefer') and (tag1[1] == 'ADV')]
	print set(adverbs)

# prob_21()

# 22
# We defined the regexp_tagger that can be used as a fall-back tagger for unknown words. This tagger only checks for cardinal numbers. 
# By testing for particular prefix or suffix strings, it should be possible to guess other tags. For example, we could tag any word 
# that ends with -s as a plural noun. Define a regular expression tagger (using RegexpTagger()) that tests for at least five other 
# patterns in the spelling of words. (Use inline documentation to explain the rules.)

def prob_22():
	patterns = [
	    (r'.*er$', 'ADJ'),     # -er = adjective
	    (r'.*ist$', 'NN'),     # -ist = person
	    (r'.*able$', 'ADJ'),   # -able = adjective
	    (r'.*ous$', 'ADJ'),    # -ous = adjective
	    (r'.*ize$', 'VBZ'),]    # -ize = verb  
	regexp_tagger = nltk.RegexpTagger(patterns)

# 23
# Consider the regular expression tagger developed in the exercises in the previous section. Evaluate the tagger using its 
# accuracy() method, and try to come up with ways to improve its performance. Discuss your findings. How does objective evaluation 
# help in the development process?

# Objective evaluation helps to see if the example patterns are used for other POS that you hadn't considered previously.
# The accuracy is very low, but it does get some of the words correctly.  A way to improve performance would to look at bigrams and
# make sure that the parts of speech being assigned make sense (make sure we are assigning an adjective after a noun, for instance).
def prob_23():
	patterns = [
	    (r'.*er$', 'ADJ'),     # -er = adjective
	    (r'.*ist$', 'NN'),     # -ist = person
	    (r'.*able$', 'ADJ'),   # -able = adjective
	    (r'.*ous$', 'ADJ'),    # -ous = adjective
	    (r'.*ize$', 'VBZ'),]    # -ize = verb  
	regexp_tagger = nltk.RegexpTagger(patterns)
	print regexp_tagger.evaluate(nltk.corpus.brown.tagged_sents(categories='news'))

# prob_23()

# 24
# How serious is the sparse data problem? Investigate the performance of n-gram taggers as n increases from 1 to 6. Tabulate the 
# accuracy score. Estimate the training data required for these taggers, assuming a vocabulary size of 105 and a tagset size of 102.
def prob_24():
	brown_tagged_sents = nltk.corpus.brown.tagged_sents(categories='news')
	size = int(len(brown_tagged_sents) * 0.9)
	train_sents = brown_tagged_sents[:size]
	test_sents = brown_tagged_sents[size:]

	for i in range(1, 7):
		ngram_tagger = nltk.NgramTagger(i, train_sents)
		print ngram_tagger.evaluate(test_sents)

# prob_24()

# 25
# Obtain some tagged data for another language, and train and evaluate a variety of taggers on it. If the language is morphologically 
# complex, or if there are any orthographic clues (e.g. capitalization) to word classes, consider developing a regular expression 
# tagger for it (ordered after the unigram tagger, and before the default tagger). How does the accuracy of your tagger(s) compare 
# with the same taggers run on English data? Discuss any issues you encounter in applying these methods to the language.

def prob_25():
	train_len = int(0.9 * len(nltk.corpus.floresta.tagged_sents()))
	train_sents = nltk.corpus.floresta.tagged_sents()[:train_len]
	test_sents = nltk.corpus.floresta.tagged_sents()[train_len:]
	unigram_tagger = nltk.UnigramTagger(train_sents)
	print unigram_tagger.evaluate(test_sents)    #accuracy: 0.767800771659

	# compare to english:
	brown_train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
	brown_train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:brown_train_len]
	brown_test_sents = nltk.corpus.brown.tagged_sents(categories='news')[brown_train_len:]
	unigram_tagger = nltk.UnigramTagger(brown_train_sents)
	print unigram_tagger.evaluate(brown_test_sents)  #accuracy: 0.812020332901

# prob_25()

# 26
# 4.1 plotted a curve showing change in the performance of a lookup tagger as the model size was increased. Plot the performance 
# curve for a unigram tagger, as the amount of training data is varied.

def performance(size):
    brown_train_len = int(size * len(nltk.corpus.brown.tagged_sents(categories='news')))
    test_len = int(0.1 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    brown_train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:brown_train_len]
    brown_test_sents = nltk.corpus.brown.tagged_sents(categories='news')[test_len:]
    unigram_tagger = nltk.UnigramTagger(brown_train_sents)
    return unigram_tagger.evaluate(brown_test_sents)

def prob_26():
    sizes = [0.1, 0.3, 0.5, 0.7, 0.9]
    perfs = [performance(size) for size in sizes]
    pylab.plot(sizes, perfs, '-bo')
    pylab.title('Unigram Tagger Performance with Varying Model Size')
    pylab.xlabel('Model Size')
    pylab.ylabel('Performance')
    pylab.show()

# prob_26()

# 27
# Inspect the confusion matrix for the bigram tagger t2 defined in 5, and identify one or more sets of tags to collapse. 
# Define a dictionary to do the mapping, and evaluate the tagger on the simplified data.

def prob_27a():
    train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:train_len]
    test_sents = nltk.corpus.brown.tagged_sents(categories='editorial')
    t0 = nltk.DefaultTagger('NN')
    t1 = nltk.UnigramTagger(train_sents, backoff=t0)
    t2 = nltk.BigramTagger(train_sents, backoff=t1)
    print t2.evaluate(test_sents)    # 0.845091227842
    test_tags = [tag for sent in nltk.corpus.brown.sents(categories='editorial') for (word, tag) in t2.tag(sent)]
    gold_tags = [tag for (word, tag) in nltk.corpus.brown.tagged_words(categories='editorial')]
    print(nltk.ConfusionMatrix(gold_tags, test_tags))

def prob_27b():
    collapse = {'NNS': 'NN', 'JJ': 'NN', 'NP': 'NN'}
    train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:train_len]
    test_sents = nltk.corpus.brown.tagged_sents(categories='editorial')
    t0 = nltk.DefaultTagger('NN')
    t1 = nltk.UnigramTagger(train_sents, backoff=t0)
    t2 = nltk.BigramTagger(train_sents, backoff=t1)
    print t2.evaluate(test_sents)
    test_tags = []
    for sent in nltk.corpus.brown.sents(categories='editorial'):
        tagged = t2.tag(sent)
        for (word, tag) in t2.tag(sent):
            if (tag in collapse.keys()):
                tag = collapse[tag]
            test_tags.append(tag)
    # test_tags = [tag for sent in nltk.corpus.brown.sents(categories='editorial') for (word, tag) in t2.tag(sent)]
    gold_tags = [tag for (word, tag) in nltk.corpus.brown.tagged_words(categories='editorial')]
    print(nltk.ConfusionMatrix(gold_tags, test_tags))

    # doesn't perform better.

# prob_27a()
# prob_27b()

# 28
# Experiment with taggers using the simplified tagset (or make one of your own by discarding all but the first character of each 
# tag name). Such a tagger has fewer distinctions to make, but much less information on which to base its work. Discuss your 
# findings.

def prob_28():
    brown_train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    brown_train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:brown_train_len]
    brown_test_sents = nltk.corpus.brown.tagged_sents(categories='news')[brown_train_len:]

    simplified_tags = []
    for sent in brown_train_sents:
        simplified_sent = []
        for (word, tag) in sent:
            simplified_sent.append((word, tag[0]))
        simplified_tags.append(simplified_sent)

    test_simplified_tags = []
    for sent in brown_test_sents:
        simplified_sent = []
        for (word, tag) in sent:
            simplified_sent.append((word, tag[0]))
        test_simplified_tags.append(simplified_sent)

    unigram_tagger = nltk.UnigramTagger(simplified_tags)

    print unigram_tagger.evaluate(test_simplified_tags)     # 0.841223960929

# prob_28()


# 29
# Recall the example of a bigram tagger which encountered a word it hadn't seen during training, and tagged the rest of the sentence 
# as None. It is possible for a bigram tagger to fail part way through a sentence even if it contains no unseen words (even if the 
# sentence was used during training). In what circumstance can this happen? Can you write a program to find some examples of this?

# If the words are used in different bigrams, this can still be an issue.  It is only completely unavoidable if the sentences
# during training and testing are exactly the same.  You could just check if the training corpus and the test corpus are the same
# and that would tell you if this could be a potential issue.

# 30
# Preprocess the Brown News data by replacing low frequency words with UNK, but leaving the tags untouched. Now train and evaluate a 
# bigram tagger on this data. How much does this help? What is the contribution of the unigram tagger and default tagger now?

def prob_30():
    tagged_words = nltk.corpus.brown.tagged_words(categories='news')
    just_words = [word for word, tag in tagged_words]

    # 75 most frequent words
    fd = nltk.FreqDist(just_words)
    common_words = fd.most_common(75)
    print common_words

    brown_train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    brown_train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:brown_train_len]
    brown_test_sents = nltk.corpus.brown.tagged_sents(categories='news')[brown_train_len:]

    fixed_train_sents = []
    for sent in brown_train_sents:
        fixed_sent = []
        for (word, tag) in sent:
            if word not in common_words:
                word = 'UNK'
            fixed_sent.append((word, tag))
        fixed_train_sents.append(fixed_sent)

    fixed_test_sents = []
    for sent in brown_test_sents:
        fixed_sent = []
        for (word, tag) in sent:
            if word not in common_words:
                word = 'UNK'
            fixed_sent.append((word, tag))
        fixed_test_sents.append(fixed_sent)
    unigram_tagger = nltk.UnigramTagger(fixed_train_sents)
    print unigram_tagger.evaluate(fixed_test_sents)     # 0.126283265225
# Very poor performance because it relies on the unigram tagger and the default tagger much more often.

# prob_30()

# 31
# Modify the program in 4.1 to use a logarithmic scale on the x-axis, by replacing pylab.plot() with pylab.semilogx(). What do you 
# notice about the shape of the resulting plot? Does the gradient tell you anything?

def performance(cfd, wordlist):
    lt = dict((word, cfd[word].max()) for word in wordlist)
    baseline_tagger = nltk.UnigramTagger(model=lt, backoff=nltk.DefaultTagger('NN'))
    return baseline_tagger.evaluate(nltk.corpus.brown.tagged_sents(categories='news'))

def prob_31():
    word_freqs = nltk.FreqDist(nltk.corpus.brown.words(categories='news')).most_common()
    words_by_freq = [w for (w, _) in word_freqs]
    cfd = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words(categories='news'))
    sizes = 2 ** pylab.arange(15)
    perfs = [performance(cfd, words_by_freq[:size]) for size in sizes]
    pylab.plot(sizes, perfs, '-bo')
    pylab.xscale('log')
    pylab.title('Lookup Tagger Performance with Varying Model Size')
    pylab.xlabel('Model Size')
    pylab.ylabel('Performance')
    pylab.show()

# The shape of the resulting plot is linear.
# prob_31()

# 32
# Consult the documentation for the Brill tagger demo function, using help(nltk.tag.brill.demo). Experiment with the tagger by setting different values for the parameters. Is there any trade-off between training time (corpus size) and performance?

def prob_32():
    demo = nltk.tag.brill.nltkdemo18()
    trainer = nltk.brill_trainer.BrillTaggerTrainer(initial_tagger=nltk.DefaultTagger('NN'), templates=demo, trace=3, deterministic=True)
    
    brown_train_len = int(0.4 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    brown_train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:brown_train_len]
    brown_test_sents = nltk.corpus.brown.tagged_sents(categories='news')[brown_train_len:]
    
    tagger = trainer.train(brown_train_sents, max_rules=20, min_score=2, min_acc=None)
    print tagger.evaluate(brown_test_sents)


# prob_32()

# 33
# Write code that builds a dictionary of dictionaries of sets. Use it to store the set of POS tags that can follow a given word 
# having a given POS tag.
def prob_33():
    tagged_words = nltk.corpus.brown.tagged_words(categories='editorial')
    words_to_tags = {}
    # tags that nouns are most likely found after
    bigrams = nltk.bigrams(tagged_words)
    for tag1, tag2 in bigrams:
        if (tag1[0] not in words_to_tags.keys()):
            words_to_tags[tag1[0]] = {}
        if (tag1[1] not in words_to_tags[tag1[0]].keys()):
            words_to_tags[tag1[0]][tag1[1]] = set()
        words_to_tags[tag1[0]][tag1[1]].add(tag2[1])

# prob_33()

# 34
# There are 264 distinct words in the Brown Corpus having exactly three possible tags.
# Print a table with the integers 1..10 in one column, and the number of distinct words in the corpus having 1..10 distinct tags in the other column.
# For the word with the greatest number of distinct tags, print out sentences from the corpus containing the word, one for each possible tag.
def prob_34():
    tags = {}
    tagged_words = nltk.corpus.brown.tagged_words()
    for word, tag in tagged_words:
        if word not in tags:
            tags[word] = set()
        tags[word].add(tag)

    counts = defaultdict(int)
    for word in tags.keys():
        num_tags = len(tags[word])
        counts[num_tags] += 1
        if num_tags == 12:
            print word      # word is "that"

    for count in counts.keys():
        sys.stdout.write(str(count)) # with no space at the end ******
        sys.stdout.write(' ')
        sys.stdout.write(str(counts[count]))
        sys.stdout.write("\n")

    seen_tags = []
    for sent in nltk.corpus.brown.tagged_sents():
        if (len(seen_tags) == 12):
            break
        for word, tag in sent:
            if word == "that" and tag not in seen_tags:
                print sent
                seen_tags.append(tag)

# prob_34()

# 35
# Write a program to classify contexts involving the word must according to the tag of the following word. Can this be used to 
# discriminate between the epistemic and deontic uses of must?

def prob_35(epistemic_tags, deontic_tags):
    tagged_sents = nltk.corpus.brown.tagged_sents(categories='editorial')
    for sent in tagged_sents:
        index = 0
        for word, tag in sent:
            if word == "must" or word == "Must":
                (word2, tag2) = sent[index + 1]
                if tag2 in epistemic_tags:
                    print "epistemic"
                elif tag2 in deontic_tags:
                    print "deontic"
                else:
                    print "unclear"
            index += 1

# prob_35(['VB', 'NP-HL'], ['BE', 'HV'])

# 36
# Create a regular expression tagger and various unigram and n-gram taggers, incorporating backoff, and train them on part of the Brown corpus.
# Create three different combinations of the taggers. Test the accuracy of each combined tagger. Which combination works best?
# Try varying the size of the training corpus. How does it affect your results?

def prob_36(training_size):
    train_len = int(training_size * len(nltk.corpus.brown.tagged_sents(categories='news')))
    train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:train_len]
    test_sents = nltk.corpus.brown.tagged_sents(categories='news')[train_len:]

    t0 = nltk.DefaultTagger('NN')
    t1 = nltk.UnigramTagger(train_sents, backoff=t0)
    t2 = nltk.BigramTagger(train_sents, backoff=t1)

    patterns = [(r'.*ing$', 'VBG'),
    (r'.*ed$', 'VBD'),
    (r'.*es$', 'VBZ'),
    (r'.*ould$', 'MD'),
    (r'.*\'s$', 'NN$'),
    (r'.*s$', 'NNS'),
    (r'^-?[0-9]+(.[0-9]+)?$', 'CD'),
    (r'.*', 'NN')]

    regexp_tagger = nltk.RegexpTagger(patterns, backoff=t1)
    regexp_tagger2 = nltk.RegexpTagger(patterns, backoff=t2)

    print t2.evaluate(test_sents)
    print regexp_tagger.evaluate(test_sents)
    print regexp_tagger2.evaluate(test_sents)

    # size = 0.3
    # 0.777439328162
    # 0.198960928048
    # 0.198960928048

    # size = 0.8
    # 0.835300351655
    # 0.196830290476
    # 0.196830290476

# prob_36(0.8)

# 37
# Our approach for tagging an unknown word has been to consider the letters of the word (using RegexpTagger()), or to ignore the word 
# altogether and tag it as a noun (using nltk.DefaultTagger()). These methods will not do well for texts having new words that are not
# nouns. Consider the sentence I like to blog on Kim's blog. If blog is a new word, then looking at the previous tag (TO versus NP$) 
# would probably be helpful. I.e. we need a default tagger that is sensitive to the preceding tag.
# Create a new kind of unigram tagger that looks at the tag of the previous word, and ignores the current word. (The best way to do this
#  is to modify the source code for UnigramTagger(), which presumes knowledge of object-oriented programming in Python.)
# Add this tagger to the sequence of backoff taggers (including ordinary trigram and bigram taggers that look at words), right before 
# the usual default tagger.
# Evaluate the contribution of this new unigram tagger.

class PreviousTagger(nltk.UnigramTagger):
    def context(self, tokens, index, history):
        if index == 0:
            return None
        else:
            return history[index-1]


def prob_37():
    train_len = int(0.9 * len(nltk.corpus.brown.tagged_sents(categories='news')))
    train_sents = nltk.corpus.brown.tagged_sents(categories='news')[:train_len]
    test_sents = nltk.corpus.brown.tagged_sents(categories='news')[train_len:]

    t0 = nltk.DefaultTagger('NN')
    t1 = PreviousTagger(train_sents, backoff=t0)
    t2 = nltk.UnigramTagger(train_sents, backoff=t1)
    t3 = nltk.BigramTagger(train_sents, backoff=t2)
    t4 = nltk.TrigramTagger(train_sents, backoff=t3)

    print t4.evaluate(test_sents)    # 0.833349945181

# prob_37()

# 38 -> link is broken, but correct tagging can be dependent on things like pronunciation, emphasis, inflection, and pragmatics.

# Stuck on 39-42.
