import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
from nltk.book import *


# Chapter 1: Language Processing and Python

# 1
# print (2*3+4)
# print (10**2)

# 2
# There are 26 ** 100 hundred-letter strings we can form.

# 3
# The elements in the list are repeated the number of times that the list is multiplied.

# 4
# print len(text2) --> 141576
# print len(set(text2)) --> 6833

# 5
# Humor is more lexically diverse than romance fiction.

# 6
# text2.dispersion_plot(["Elinor", "Marianne", "Edward", "Willoughby"])
# Elinor and Marianne show up consistently throughout the entire novel, so we can assume that the plot revolves
# around them moreso than the men.  Edwards and Willoughby show up at almost exactly opposite points
# (meaning that if one is mentioned, the other is not).  It seems that Marianna and Willoughby are a couple
# because they seem to appear at the same times.

# 7
# text5.collocations() --> wanna chat; PART JOIN; MODE #14-19teens; JOIN PART; PART PART;
# cute.-ass MP3; MP3 player; JOIN JOIN; times .. .; ACTION watches; guys
# wanna; song lasts; last night; ACTION sits; -...)...- S.M.R.; Lime
# Player; Player 12%; dont know; lez gurls; long time

# 8
# The expression counts all unique words in text4.  set(text4) creates the set of all unique words in text4, and
# the len function counts the words in that set.

# 9
# example_string = 'hello'
# print example_string
# fix for no spaces: example_string + ' ' + example_string

# 10
# example_list = ['hello', 'world']
# joined = ' '.join(example_list)
# print joined

# print joined.split()

# 11
# phrase1 = ['my', 'name', 'is']
# phrase2 = ['Abbey']
# phrase3 = ['Hello', ',']

# combined = phrase3 + phrase1 + phrase2
# print combined

# len(phrase1 + phrase2) combines the two lists of words first, then calculates the length of the combined list.
# len(phrase1) + len(phrase2) calculates the length of the two lists separately, then adds their values.

# 12
# b will be more relevant in NLP because texts are represented as lists of strings, not as a string itself.

# 13
# sent1[2][2] selects the third character from the third word in the list sent1, because characters in a string are
# also indexed, like lists.

# 14
# 5, 8

# 15
# print sorted(set(w for w in text5 if w.startswith('b')))

# 16
# print list(range(10))
# print list(range(10, 20))
# print list(range(10, 20, 2))
# print list(range(20, 10, -2))

# 17
# print text9.index('sunset')
# print text9[621:644]

# 18
# sentences = [sent1, sent2, sent3, sent4, sent5, sent6, sent7, sent8]
# vocab = []
# for sent in sentences:
# 	vocab += [w for w in sent]
# vocab = sorted(set(vocab))
# print vocab

# 19
# sorted(set(w.lower() for w in text1)) takes the set of words once all words have been converted to lowercase.
# sorted(w.lower() for w in set(text1)) takes the set of words from the text first, then converts them to lowercase.
# The second line will give a larger list because words that are seen both in lowercase and uppercase will be included.
# The first line will never have duplicates.

# 20
# w.isupper() checks if a word is uppercase.  not w.islower() checks if the word is lowercase, then takes the opposite
# result.

# 21
# print text2[-2:]

# 22
# words = set(w for w in text5 if len(w) == 4)
# fdist = FreqDist(words)
# print fdist

# 23
# for word in text6:
# 	if word.isupper():
# 		print word

# 24
# a = [w for w in text6 if w.endswith('ize')]
# print a

# b = [w for w in text6 if 'z' in w]
# print b

# c = [w for w in text6 if 'pt' in w]
# print c

# d = [w for w in text6 if w.istitle()]
# print d

# 25
# sent = ['she', 'sells', 'sea', 'shells', 'by', 'the', 'sea', 'shore']
# for word in sent:
# 	if word.startswith('sh'):
# 		print word
# for word in sent:
# 	if len(word) > 4:
# 		print word

# 26
# The expression sums the length of all the words in text1.  Dividing the result by the total number of words, or
# len(text1), will give the average word length.

# 27
# def vocab_size(text):
# 	return len(set(text))

# 28
# def percent(word, text):
# 	return 100 * text.count(word) / len(text)

# 29
# a < b checks if set a is a subset of set b and b has at least one additional element not in a.  
# This could be used for comparing lexical diversity.

