import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/")
import nltk
from nltk import corpus
from nltk.corpus import wordnet as wn
# from nltk.book import text5
from nltk.corpus import brown
import random
import pylab
from decimal import *

# Textbook solutions for Chapter 2

# 1
# phrase = ['Hello', 'my', 'name', 'is']
# phrase2 = ['Abbey']
# addition
# print phrase + phrase2
# multiplication
# print phrase * 2
# indexing
# print phrase[3]
# slicing
# print phrase[:2]
# sorting
# print sorted(phrase)

# 2
# print len(corpus.gutenberg.words('austen-persuasion.txt'))
# ****how many word types???

# 3
# print corpus.brown.words(categories='news')[:10]
# print corpus.brown.words(categories='fiction')[:10]

# 4

# cfd = nltk.ConditionalFreqDist(
# 	(target, fileid[:4])
# 	for fileid in corpus.state_union.fileids()
# 	for w in corpus.state_union.words(fileid)
# 	for target in ['men', 'women', 'people']
# 	if w.lower().startswith(target))

# cfd.plot()

# ANSWER: The frequency of 'women' and 'men' has increased over time.  The frequency of 'people' has 
# always been higher.
# The frequency of 'men' used to be greater than 'women', but they are now equal.

# 5

# Investigate the holonym-meronym relations for some nouns. 
# Remember that there are three kinds of holonym-meronym relation, so you need to use: 
# member_meronyms(), part_meronyms(),  substance_meronyms(), member_holonyms(), 
# part_holonyms(), and substance_holonyms().

# print wn.synset('bed.n.01').definition()
# a piece of furniture that provides a place to sleep
# print wn.synset('bed.n.01').part_meronyms()
# [Synset('bedstead.n.01'), Synset('mattress.n.01')]
# print wn.synset('mattress.n.01').member_meronyms
# <bound method Synset.member_meronyms of Synset('mattress.n.01')>
# print wn.synset('bird.n.01').member_holonyms()
# [Synset('aves.n.01'), Synset('flock.n.02')]
# print wn.synset('bronze.n.01').substance_meronyms()
# [Synset('copper.n.01')]
# print wn.synset('copper.n.01').substance_holonyms()
# [Synset('bornite.n.01'), Synset('brass.n.01'), Synset('bronze.n.01'), Synset('chalcocite.n.01'), Synset('chalcopyrite.n.01'), Synset('cuprite.n.01'), Synset('malachite.n.01')]
# print wn.synset('petal.n.01').part_holonyms()
# [Synset('corolla.n.01')]

 #6

 # In the discussion of comparative wordlists, we created an object called translate which you could 
 # look up using words in both German and Spanish in order to get corresponding words in English.
 # What problem might arise with this approach? Can you suggest a way to avoid this problem?

# It could be problematic if there are false cognates in the two languages.  Because the dictionary
# lookup does not check the language of the original word, there is a chance that the word will be 
# translated from wrong language.  A way to fix this would be to force the user to specify the language
# of the word to be translated, or just keeping the translations in separate dictionaries.

# 7
# According to Strunk and White's Elements of Style, the word however, used at the start of a sentence, 
# means "in whatever way" or "to whatever extent", and not "nevertheless". They give this example of correct
#  usage: However you advise him, he will probably do as he thinks best. (http://www.bartleby.com/141/strunk3
#  	.html) Use the concordance tool to study actual usage of this word in the various texts we have been 
#  considering.

# print nltk.Text(nltk.corpus.gutenberg.words('austen-persuasion.txt')).concordance('However')
# print nltk.Text(nltk.corpus.gutenberg.words('austen-emma.txt')).concordance('However')
# print nltk.Text(nltk.corpus.gutenberg.words('austen-emma.txt')).concordance('However')

# 8

# Define a conditional frequency distribution over the Names corpus that allows you to see which initial 
# letters are more frequent for males vs. females.

# cfd = nltk.ConditionalFreqDist(
# 		(fileid, name[0])
# 		for fileid in nltk.corpus.names.fileids()
# 		for name in nltk.corpus.names.words(fileid))
# cfd.plot()

# 9

# Pick a pair of texts and study the differences between them, in terms of vocabulary, vocabulary 
# richness, genre, etc. Can you find pairs of words which have quite different meanings across the
#  two texts, such as monstrous in Moby Dick and in Sense and Sensibility?
# fileids = ['carroll-alice.txt', 'austen-emma.txt']

# for fileid in fileids:
# 	num_chars = len(corpus.gutenberg.raw(fileid))
# 	num_words = len(corpus.gutenberg.words(fileid))
# 	num_sents = len(corpus.gutenberg.sents(fileid))
# 	num_vocab = len(set(w.lower() for w in corpus.gutenberg.words(fileid)))
# 	print(round(num_chars/num_words), round(num_words/num_sents), round(num_words/num_vocab), fileid)

# emma = nltk.Text(nltk.corpus.gutenberg.words('austen-emma.txt'))
# alice = nltk.Text(nltk.corpus.gutenberg.words('carroll-alice.txt'))
# print emma.similar('heart')
# print alice.similar('heart')

# 10
# How many word types account for a third of all word tokens, for a variety of text sources? What 
# do you conclude about this statistic?
# print 100.0 * text5.count('a') / len(text5)
# print 100.0 * text5.count('the') / len(text5)
# print 100.0 * text5.count('i') / len(text5)
# alice = nltk.Text(nltk.corpus.gutenberg.words('carroll-alice.txt'))
# print 100.0 * alice.count('a') / len(alice)
# print 100.0 * alice.count('the') / len(alice)
# print 100.0 * alice.count('I') / len(alice)

# 0.  This statistic is probably never correct for written text.  If simple filler words like 'a', 'the',
# and 'i' only account for less than 2% of all word tokens in the chat corpus, it is extremely
# unlikely that any other words account for a third of all word tokens in any corpus.

# 11
# Investigate the table of modal distributions and look for other patterns. Try to explain them 
# in terms of your own impressionistic understanding of the different genres. Can you find other 
# closed classes of words that exhibit significant differences across different genres?

# Hobbies use many modals, which makes sense because the texts probably include lots of instructions.
# Might is used least frequently because hobby texts are written in definites.

# cfd = nltk.ConditionalFreqDist(
# 	(genre, word)
# 	for genre in brown.categories()
# 	for word in brown.words(categories=genre))
# genres = ['news', 'religion', 'hobbies', 'science_fiction', 'romance', 'humor']
# first_p_pronouns = ['I', 'me', 'we', 'us']
# cfd.tabulate(conditions=genres, samples=first_p_pronouns)

# Romance and humor use 'I' often.  Science fiction rarely uses 'us'.

# 12
# The CMU Pronouncing Dictionary contains multiple pronunciations for certain words. 
# How many distinct words does it contain? What fraction of words in this dictionary 
# have more than one possible pronunciation?
# print len(set(nltk.corpus.cmudict.dict().keys())) # distinct words
# print (1.0 - (len(set(nltk.corpus.cmudict.dict().keys())) * 1.0 / len(nltk.corpus.cmudict.dict().keys())))


# 13
# What percentage of noun synsets have no hyponyms? You can get all noun synsets using wn.all_synsets('n').
# num_no_hyponyms = 0
# total = 0
# for w in wn.all_synsets('n'):
# 	if len(w.hyponyms()) == 0:
# 		num_no_hyponyms += 1
# 	total += 1
# print 1.0 * num_no_hyponyms / total 
# 0.796711928393

# 14
# Define a function supergloss(s) that takes a synset s as its argument and returns a string 
# consisting of the concatenation of the definition of s, and the definitions of all the 
# hypernyms and hyponyms of s.

def supergloss(s):
	gloss = s.definition()
	for hypernym in s.hypernyms():
		gloss += hypernym.definition()
	for hyponym in s.hyponyms():
		gloss += hyponym.definition()
	return gloss

# 15
# Write a program to find all words that occur at least three times in the Brown Corpus.
# word_list = [word for word in set(brown.words()) if brown.words().count(word) >= 3]

# 16
# Write a program to generate a table of lexical diversity scores (i.e. token/type ratios), 
# as we saw in 1.1. Include the full set of Brown Corpus genres (nltk.corpus.brown.categories()). 
# Which genre has the lowest diversity (greatest number of tokens per type)? Is this what you 
# would have expected?
# for category_name in brown.categories():
# 	num_words = len(brown.words(categories=category_name))
# 	num_sents = len(brown.sents(categories=category_name))
# 	num_vocab = len(set(w.lower() for w in brown.words(categories=category_name)))
# 	print(round(num_words/num_sents), round(num_words/num_vocab), category_name)

# learned has the lowest diversity.  I would have expected it to be editorial or news.

# 17
# Write a function that finds the 50 most frequently occurring words of a text that are not stopwords.
def freq_occurring(text):
	stopwords = nltk.corpus.stopwords.words('english')
	content = [w for w in text if w.lower() not in stopwords]
	fd = nltk.FreqDist(content)
	return fd.most_common(50)

# 18
# Write a program to print the 50 most frequent bigrams (pairs of adjacent words) of a text, 
# omitting bigrams that contain stopwords. 
def freq_occurring_bigrams(text):
	stopwords = nltk.corpus.stopwords.words('english')
	content = [bigram for bigram in nltk.bigrams(text) if bigram[0].lower() not in stopwords and bigram[1].lower() not in stopwords]
	fd = nltk.FreqDist(content)
	print fd.most_common(50)
	return

# 19
# Write a program to create a table of word frequencies by genre, like the one given in 1 
# for modals. Choose your own words and try to find words whose presence (or absence) is 
# typical of a genre. Discuss your findings.
# cfd = nltk.ConditionalFreqDist(
# 	(genre, word)
# 	for genre in brown.categories()
# 	for word in brown.words(categories=genre))
# genres = ['news', 'religion', 'hobbies', 'science_fiction', 'romance', 'humor']
# words = ['charged', 'God', 'love', 'laugh']
# cfd.tabulate(conditions=genres, samples=words)

# God and love are both used frequently in religion and romance.  They are not common in news or hobbies.

# 20
# Write a function word_freq() that takes a word and the name of a section of the Brown Corpus
#  as arguments, and computes the frequency of the word in that section of the corpus.
def word_freq(word, section):
	genre_words = brown.words(categories=section)
	num_occurrence = brown.words(categories=section).count(word)
	return num_occurrence * 1.0 / len(genre_words)

# 21
# Write a program to guess the number of syllables contained in a text, making use of the 
# CMU Pronouncing Dictionary.
# def guess_num_syllables(text):
# This is not possible.  The CMU Pronouncing Dictionary (as described in the chapter) does not
# have any information on syllable processing for its entries.

# 22
# Define a function hedge(text) which processes a text and produces a new version with 
# the word 'like' between every third word.

def hedge(text):
	new_text = ''
	index = 0
	for w in text:
		if (index != 0 and index % 3 == 0):
			new_text.append('like')
		new_text.append(w)
		index += 1

# 23
def freq_rank(text):
	frequencies = []
	ranks = []
	fdist = nltk.FreqDist([w.lower() for w in text])
	keys = fdist.keys()
	n = 1
	for w in keys:
	    frequency = Decimal.logb(Decimal(fdist[w]))
	    frequencies.append(frequency)
	for w in keys:
	    ranks.append(Decimal.logb(Decimal(n)))
	    n = n + 1

	pylab.plot(ranks, frequencies)
	return pylab.show()

# freq_rank(brown.words(categories='news'))


# 24
# Modify the text generation program in 2.2 further, to do the following tasks:
# Store the n most likely words in a list words then randomly choose a word from the list 
# using random.choice(). 

# Select a particular genre, such as a section of the Brown Corpus, or a genesis translation, 
# one of the Gutenberg texts, or one of the Web texts. Train the model on this corpus and get 
# it to generate random text. You may have to experiment with different start words. How 
# intelligible is the text? Discuss the strengths and weaknesses of this method of generating random text.

def generate_model(cfdist, word, n, num=15):
    for i in range(num):
        print(word + ', ')
        likely_words = cfdist[word].keys()[:n]
        word = random.choice(likely_words)

# text1 = nltk.corpus.genesis.words('english-kjv.txt')
# text2 = brown.words(categories='news')
# text = text1 + text2
# bigrams = nltk.bigrams(text)
# cfd = nltk.ConditionalFreqDist(bigrams)
# cfd['living']
# generate_model(cfd, 'living', 5)

# Some parts of the text make sense (groups of 3-4 words), but in general, the text in not intelligible.
# This method of generating random text is easy and simple to implement, but it does not produce
# realistic or sensical text, in general.

# Now train your system using two distinct genres and experiment with generating text 
# in the hybrid genre. Discuss your observations.

# It seems that the generated text switches back and forth between the two genres every few words.
# It is still nonsensical and difficult to understand.

# 25
# Define a function find_language() that takes a string as its argument, and returns a list of 
# languages that have that string as a word. Use the udhr corpus and limit your searches to files 
# in the Latin-1 encoding.

def find_language(word):
	langs = []
	for fileid in nltk.corpus.udhr.fileids():
		if 'Latin-1' in fileid:
			if word in nltk.corpus.udhr.words(fileid):
				langs.append(fileid)

# 26
# What is the branching factor of the noun hypernym hierarchy? I.e. for every noun synset that has 
# hyponyms or children in the hypernym hierarchy how many do they have on average? You can get 
# all noun synsets using wn.all_synsets('n').

def calc_branching_factor():
	num_hyponyms = 0
	num_synsets = 0
	for synset in wn.all_synsets('n'):
		if len(synset.hyponyms()) > 0:
			num_hyponyms += len(synset.hyponyms())
			num_synsets += 1
	return 1.0 * num_hyponyms / num_synsets

# 27
# The polysemy of a word is the number of senses it has. Using WordNet, we can determine that the 
# noun dog has 7 senses with: len(wn.synsets('dog', 'n')). Compute the average polysemy of nouns, 
# verbs, adjectives and adverbs according to WordNet.

def avg_polysemy(pos):
	num_senses = 0
	num_synsets = 0
	for synset in wn.all_synsets(pos):
		for lemma in synset.lemmas():
			ex_word = lemma.name
			num_senses += len(wn.synsets(ex_word, pos))
			num_synsets += 1
	return 1.0 * num_senses / num_synsets

# print avg_polysemy('n')

# 28
# Use one of the predefined similarity measures to score the similarity of each of the following 
# pairs of words. Rank the pairs in order of decreasing similarity. How close is your ranking to 
# the order given here?

def calc_path_similarity(word1, word2):
	synset1 = wn.synset(word1 + '.n.01')
	synset2 = wn.synset(word2 + '.n.01')
	print synset1.path_similarity(synset2)
	return


def calc_all_similarities():
	similarities = []
	similarities.append(calc_path_similarity('car', 'automobile'))
	similarities.append(calc_path_similarity('gem', 'jewel'))
	similarities.append(calc_path_similarity('journey', 'voyage')) 
	similarities.append(calc_path_similarity('boy', 'lad'))
	similarities.append(calc_path_similarity('coast', 'shore'))
	similarities.append(calc_path_similarity('asylum', 'madhouse'))
	similarities.append(calc_path_similarity('magician', 'wizard'))
	similarities.append(calc_path_similarity('midday', 'noon'))
	similarities.append(calc_path_similarity('furnace', 'stove'))
	similarities.append(calc_path_similarity('food', 'fruit'))
	similarities.append(calc_path_similarity('bird', 'cock'))
	similarities.append(calc_path_similarity('bird', 'crane'))
	similarities.append(calc_path_similarity('tool', 'implement'))
	similarities.append(calc_path_similarity('lad', 'brother'))
	similarities.append(calc_path_similarity('crane', 'implement'))
	similarities.append(calc_path_similarity('journey', 'car'))
	similarities.append(calc_path_similarity('monk', 'oracle'))
	similarities.append(calc_path_similarity('cemetery', 'woodland'))
	similarities.append(calc_path_similarity('food', 'rooster'))
	similarities.append(calc_path_similarity('coast', 'hill'))
	similarities.append(calc_path_similarity('forest', 'graveyard'))
	similarities.append(calc_path_similarity('shore', 'woodland'))
	similarities.append(calc_path_similarity('monk', 'slave'))
	similarities.append(calc_path_similarity('coast', 'forest'))
	similarities.append(calc_path_similarity('lad', 'wizard'))
	similarities.append(calc_path_similarity('chord', 'smile'))
	similarities.append(calc_path_similarity('glass', 'magician'))
	similarities.append(calc_path_similarity('rooster', 'voyage'))
	similarities.append(calc_path_similarity('noon', 'string'))

# calc_all_similarities()

# The similarity judgments are very close to the provided rankings.

